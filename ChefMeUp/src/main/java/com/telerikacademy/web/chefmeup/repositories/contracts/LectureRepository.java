package com.telerikacademy.web.chefmeup.repositories.contracts;

import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.LectureSearchDto;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;

public interface LectureRepository extends BaseModifyRepository<Lecture> {

    void uploadAssignment(MultipartFile file, Lecture lecture, String name, Path assignments);

    List<Lecture> getAllCourseLectures(int courseId);

    Course getCourseByLecture(int lectureId);

    void uploadLecturePicture(MultipartFile file, Lecture newLecture, String name, Path assignments);

    List<Lecture> getAllTeacherLectures(User lu);

    List<Lecture> search(LectureSearchDto lectureSearchDto);
}
