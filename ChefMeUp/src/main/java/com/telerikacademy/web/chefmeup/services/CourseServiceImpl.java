package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.exeptions.DuplicateEntityException;
import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.chefmeup.exeptions.VisibilityNotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.StudentLecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.CourseSearchDto;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.LectureRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentLectureRepository;
import com.telerikacademy.web.chefmeup.services.contracts.CourseService;
import com.telerikacademy.web.chefmeup.services.contracts.StudentLectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class CourseServiceImpl implements CourseService {

    private static final String EXPIRE_DATE_MUST_BE_AFTER_STARING_DATE_MSG = "Expire date must be after Staring date";
    private static final String COURSE_WITH_STARTING_DATE_MUST_HAVE_EXPIRING_DATE_MSG = "Course with starting date must have and a expiring date";
    private static final String COURSE_WITH_EXPIRE_DATE_MUST_HAVE_STARTING_DATE_MSG = "Course with expiring date must have and a starting date";
    private static final String MINIMUM_TWO_LECTURES_IN_COURSE_MSG = "You must have at least 2 lectures in the course to be visible online";
    private static final String MODIFY_ONLY_CREATOR_OR_ADMIN_MSG = "To modify this course you must be the creator or an admin";
    private static final String MUST_HAVE_DATES_TO_BE_ONLINE_MSG = "Course cannot be uploaded online if there is no start and expire date";
    private static final String ALREADY_STARTED_COURSE_MSG = "You cannot update a course which is started already.";
    private static final String CANNOT_DELETE_A_COURSE_WITH_LECTURES_MSG = "You cannot delete a course with lectures in it";

    private final CourseRepository courseRepository;
    private final LectureRepository lectureRepository;
    private final StudentLectureRepository studentLectureRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository,
                             LectureRepository lectureRepository,
                             StudentLectureRepository studentLectureRepository) {
        this.courseRepository = courseRepository;
        this.lectureRepository = lectureRepository;
        this.studentLectureRepository = studentLectureRepository;
    }

    @Override
    public List<Course> getAll() {
        return courseRepository.getAll();
    }

    @Override
    public Course getById(int courseId) {
        return courseRepository.getById(courseId);
    }

    @Override
    public void create(Course course, User loggedUser) {
        if (courseRepository.doesCourseTitleExist(course.getTitle(), 0)) {
            throw new DuplicateEntityException("Course", "title", course.getTitle());
        }
        if (course.getStartingDate() != null &&
                course.getExpireDate() != null &&
                course.getExpireDate().isBefore(course.getStartingDate())) {
            throw new NotAllowedOperationException(EXPIRE_DATE_MUST_BE_AFTER_STARING_DATE_MSG);
        }
        if (course.getStartingDate() != null && course.getExpireDate() == null) {
            throw new NotAllowedOperationException(COURSE_WITH_STARTING_DATE_MUST_HAVE_EXPIRING_DATE_MSG);
        } else if (course.getStartingDate() == null && course.getExpireDate() != null) {
            throw new NotAllowedOperationException(COURSE_WITH_EXPIRE_DATE_MUST_HAVE_STARTING_DATE_MSG);
        }
        if (course.isVisible()) {
            throw new VisibilityNotAllowedOperationException(MINIMUM_TWO_LECTURES_IN_COURSE_MSG);
        }
        course.setCreator(loggedUser);

        courseRepository.create(course);
    }

    @Override
    public void update(Course course, User loggedUser) {
        if (loggedUser.getId() != course.getCreator().getId() && !course.getCreator().isAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_ONLY_CREATOR_OR_ADMIN_MSG);
        }
        if (courseRepository.doesCourseTitleExist(course.getTitle(), course.getId())) {
            throw new DuplicateEntityException("Course", "title", course.getTitle());
        }
        if (course.getStartingDate() != null &&
                course.getExpireDate() != null &&
                course.getExpireDate().isBefore(course.getStartingDate())) {
            throw new NotAllowedOperationException(EXPIRE_DATE_MUST_BE_AFTER_STARING_DATE_MSG);
        }
        if (course.isVisible() && course.getStartingDate() == null && course.getExpireDate() == null) {
            throw new VisibilityNotAllowedOperationException(MUST_HAVE_DATES_TO_BE_ONLINE_MSG);
        } else if (course.isVisible() && course.getStartingDate() != null && course.getExpireDate() == null) {
            throw new VisibilityNotAllowedOperationException(COURSE_WITH_STARTING_DATE_MUST_HAVE_EXPIRING_DATE_MSG);
        } else if (course.isVisible() && course.getStartingDate() == null && course.getExpireDate() != null) {
            throw new VisibilityNotAllowedOperationException(COURSE_WITH_EXPIRE_DATE_MUST_HAVE_STARTING_DATE_MSG);
        }
        if (course.isVisible() &&
                lectureRepository.getAllCourseLectures(course.getId()).size() < 2) {
            throw new VisibilityNotAllowedOperationException(MINIMUM_TWO_LECTURES_IN_COURSE_MSG);
        }
        if (course.isVisible() &&
                course.getStartingDate().isBefore(LocalDate.now())) {
            throw new VisibilityNotAllowedOperationException(ALREADY_STARTED_COURSE_MSG);
        }
        courseRepository.update(course);
    }

    @Override
    public void delete(int courseId) {
        if (!lectureRepository.getAllCourseLectures(courseId).isEmpty()) {
            throw new NotAllowedOperationException(CANNOT_DELETE_A_COURSE_WITH_LECTURES_MSG);
        }
        Course courseToDelete = getById(courseId);
        if (courseToDelete.isVisible() &&
                courseToDelete.getStartingDate().isBefore(LocalDate.now()) &&
                LocalDate.now().isBefore(courseToDelete.getExpireDate())) {
            throw new NotAllowedOperationException(ALREADY_STARTED_COURSE_MSG);
        }
        courseRepository.delete(courseId);
    }

    @Override
    public double getCourseAverageRating(int courseId) {
        return courseRepository.getCourseAverageRating(courseId);
    }

    @Override
    public void uploadCoursePicture(MultipartFile file, Course newCourse, User lu) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = newCourse.getTitle() + "-" +
                "BACKGROUND" + "." +
                fileName.substring(fileName.lastIndexOf(".") + 1);
        String name = saveAsName.replaceAll(" ", "");
        Path resourceDirectory = Paths.get("media", "coursebackgrounds", name);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path assignments = Paths.get(absolutePath);

        courseRepository.uploadCoursePicture(file, newCourse, name, assignments);
    }

    @Override
    public List<Course> getAllTeacherDraftCourses(User lu) {
        return courseRepository.getAllTeacherDraftCourses(lu);
    }

    @Override
    public List<Course> getAllTeacherCourses(User lu) {
        if (lu.isAdmin()) {
            return courseRepository.getAll();
        }
        return courseRepository.getAllTeacherCourses(lu);
    }

    @Override
    public List<Course> getAllUsersEnrolledCourses(User loggedUser) {
        return courseRepository.getAllUsersEnrolledCourses(loggedUser);
    }

    @Override
    public List<Course> search(CourseSearchDto courseSearchDto) {
        return courseRepository.search(courseSearchDto);
    }

    @Override
    public Map<Integer, String> getAllCourseRatings() {
        List<Course> courses = courseRepository.getAll();
        Map<Integer, String> courseRatings = new HashMap<>();

        for (Course course : courses) {
            if (courseRepository.getCourseAverageRating(course.getId()) == 0) {
                courseRatings.put(course.getId(), "No rating yet.");
                continue;
            }
            courseRatings.put(course.getId(), String.format("Rating: %.1f/5", courseRepository.getCourseAverageRating(course.getId())));
        }

        return courseRatings;
    }

    public boolean isCourseStarted(int courseId) {
        Course course = courseRepository.getById(courseId);
        return course.getStartingDate().isBefore(LocalDate.now());
    }

    @Override
    public List<Course> getAllOnlineCourses() {
        return courseRepository.getAllOnlineCourses();
    }

    @Override
    public Map<Integer, String> getUsersCourseGrade(User loggedUser) {
        List<Course> courses = courseRepository.getAllUsersEnrolledCourses(loggedUser);
        Map<Integer, String> courseGrades = new HashMap<>();
        double courseGrade = 0;
        for (Course course : courses) {
            List<Lecture> lectures = lectureRepository.getAllCourseLectures(course.getId());
            for (Lecture lecture : lectures) {
                StudentLecture studentLectures = studentLectureRepository.getByStudentAndLecture(loggedUser.getId(), lecture.getId());
                courseGrade += studentLectures.getGrade();
            }
            double avgGrade = courseGrade / lectures.size();
            courseGrades.put(course.getId(), String.format("%.1f", avgGrade));
            courseGrade = 0;
        }
        return courseGrades;
    }

}
