package com.telerikacademy.web.chefmeup.exeptions;

public class NotAllowedOperationException extends RuntimeException {
    public NotAllowedOperationException(String message) {
        super(message);
    }
}
