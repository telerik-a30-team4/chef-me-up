package com.telerikacademy.web.chefmeup.repositories.contracts;

import com.telerikacademy.web.chefmeup.models.StudentCourse;
import com.telerikacademy.web.chefmeup.models.User;

import java.util.List;

public interface StudentCourseRepository {

    List<StudentCourse> getAll();

    StudentCourse getStudentCourse(int studentId, int courseId);

    void enroll(StudentCourse studentCourse);

    StudentCourse rateCourse(StudentCourse studentCourse);

    double getStudentCourseAverageGrade(int courseId, int studentId);

    boolean isUserEnrolled(int courseId, User lu);

    boolean isCourseRated(int courseId, User lu);
}
