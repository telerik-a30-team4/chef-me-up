package com.telerikacademy.web.chefmeup.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
public class CourseDto {

    @NotBlank(message = "Course title can't be empty")
    @Size(min = 5,max = 50)
    private String title;

    @Positive
    private int topicId;

    @NotBlank(message = "Course description can't be empty")
    @Size(min = 20, max = 1000)
    private String description;

    private String startingDate;

    private String expireDate;

    @NotNull
    private boolean visible;

    private String picture;

    private String video;
}
