package com.telerikacademy.web.chefmeup.controllers.rest;

import com.telerikacademy.web.chefmeup.config.security.AppUserDetails;
import com.telerikacademy.web.chefmeup.models.StudentLecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.RegisterUserDto;
import com.telerikacademy.web.chefmeup.models.mappers.UserMapper;
import com.telerikacademy.web.chefmeup.models.params.UserSearchParams;
import com.telerikacademy.web.chefmeup.services.contracts.StudentLectureService;
import com.telerikacademy.web.chefmeup.services.contracts.UserService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final StudentLectureService studentLectureService;

    public UserRestController(UserService userService,
                              UserMapper userMapper,
                              StudentLectureService studentLectureService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.studentLectureService = studentLectureService;
    }

    @Secured("ROLE_TEACHER")
    @GetMapping
    public List<User> getAll(@AuthenticationPrincipal AppUserDetails loggedUser) {
        return userService.getAll(loggedUser.getUser());
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/{userId}")
    public User getById(@PathVariable int userId,
                        @AuthenticationPrincipal AppUserDetails loggedUser) {
        return userService.getById(userId, loggedUser.getUser());
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/search")
    public List<User> search(@RequestParam(required = false) Optional<String> nameOrEmail,
                             @RequestParam(required = false) Optional<String> orderBy,
                             @AuthenticationPrincipal AppUserDetails loggedUser) {
        UserSearchParams userSearchParams = userMapper.searchParamsToObject(nameOrEmail, orderBy);

        return userService.search(userSearchParams, loggedUser.getUser());
    }

    @PostMapping
    public User create(@Valid @RequestBody RegisterUserDto registerUserDto) {
        User user = userMapper.userFromDto(registerUserDto);
        userService.create(user);

        return user;
    }

    @Secured("ROLE_STUDENT")
    @PutMapping("/{userId}")
    public User update(@PathVariable int userId,
                       @Valid @RequestBody RegisterUserDto registerUserDto,
                       @AuthenticationPrincipal AppUserDetails loggedUser) {
        User userToUpdate = userMapper.userFromDto(registerUserDto, userId);
        userService.update(userToUpdate, loggedUser.getUser());

        return userToUpdate;
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{userId}")
    public void delete(@PathVariable int userId,
                       @AuthenticationPrincipal AppUserDetails loggedUser) {

        userService.delete(userId, loggedUser.getUser());
    }

    @Secured("ROLE_STUDENT")
    @PostMapping("/{userId}/lecture/{lectureId}/assignment")
    public void submitAssignmentToLecture(@PathVariable int userId,
                                          @PathVariable int lectureId,
                                          @RequestParam("file") MultipartFile file,
                                          @AuthenticationPrincipal AppUserDetails loggedUser) {
        StudentLecture studentLecture = studentLectureService.getByStudentAndLecture(userId, lectureId);

        studentLectureService.submitAssignmentToLecture(studentLecture, file, loggedUser.getUser());
    }
}
