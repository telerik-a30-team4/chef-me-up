package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.StudentCourse;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentCourseRepository;
import com.telerikacademy.web.chefmeup.services.contracts.StudentCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentCourseServiceImpl implements StudentCourseService {

    private static final String RATE_COURSE_ERROR_MSG = "You cannot rate course, you need to pass the minimum average grade.";
    private static final int COURSE_PASS_AVG_GRADE = 50;

    private final StudentCourseRepository studentCourseRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public StudentCourseServiceImpl(StudentCourseRepository studentCourseRepository,
                                    CourseRepository courseRepository) {
        this.studentCourseRepository = studentCourseRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<StudentCourse> getAll() {
        return studentCourseRepository.getAll();
    }

    @Override
    public StudentCourse getStudentCourse(int studentId, int courseId) {
        return studentCourseRepository.getStudentCourse(studentId, courseId);
    }

    @Override
    public void enroll(int courseId, User loggedUser) {
        StudentCourse studentCourse = new StudentCourse();
        studentCourse.setStudent(loggedUser);
        studentCourse.setCourse(courseRepository.getById(courseId));

        studentCourseRepository.enroll(studentCourse);
    }

    @Override
    public StudentCourse rateCourse(int courseId, User loggedUser, int rate) {
        StudentCourse studentCourse = studentCourseRepository.getStudentCourse(loggedUser.getId(), courseId);

        if (studentCourseRepository.getStudentCourseAverageGrade(courseId, loggedUser.getId()) < COURSE_PASS_AVG_GRADE) {
            throw new NotAllowedOperationException(RATE_COURSE_ERROR_MSG);
        }

        studentCourse.setCourseRating(rate);
        return studentCourseRepository.rateCourse(studentCourse);
    }

    @Override
    public boolean isUserEnrolled(int courseId, User loggedUser) {
        return studentCourseRepository.isUserEnrolled(courseId, loggedUser);
    }

    @Override
    public boolean isCourseRated(int courseId, User loggedUser) {
        return studentCourseRepository.isCourseRated(courseId, loggedUser);
    }

}
