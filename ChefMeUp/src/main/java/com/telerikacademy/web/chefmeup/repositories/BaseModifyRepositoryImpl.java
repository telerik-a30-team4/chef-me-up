package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.repositories.contracts.BaseModifyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public abstract class BaseModifyRepositoryImpl<T> extends BaseGetRepositoryImpl<T> implements BaseModifyRepository<T> {

    public BaseModifyRepositoryImpl(SessionFactory sessionFactory, Class<T> clazz) {
        super(sessionFactory, clazz);
    }

    @Override
    public void create(T entity) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(T entity) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.update(entity);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        T objectToDelete = getById(id);
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(objectToDelete);
            session.getTransaction().commit();
        }
    }

}
