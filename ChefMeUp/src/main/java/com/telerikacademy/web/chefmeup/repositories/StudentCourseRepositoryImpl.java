package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.exeptions.EntityNotFoundException;
import com.telerikacademy.web.chefmeup.models.StudentCourse;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentCourseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StudentCourseRepositoryImpl implements StudentCourseRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public StudentCourseRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<StudentCourse> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<StudentCourse> query = session.createQuery("from StudentCourse ", StudentCourse.class);

            return query.list();
        }
    }

    @Override
    public StudentCourse getStudentCourse(int studentId, int courseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<StudentCourse> query = session
                    .createQuery("from StudentCourse s where s.student.id = :studentId AND s.course.id = :courseId", StudentCourse.class);

            query.setParameter("studentId", studentId);
            query.setParameter("courseId", courseId);

            List<StudentCourse> studentCourse = query.list();

            if (studentCourse.size() == 0) {
                throw new EntityNotFoundException("Student", "course with id", String.valueOf(courseId));
            }

            return studentCourse.get(0);
        }
    }

    @Override
    public void enroll(StudentCourse studentCourse) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(studentCourse);
            session.getTransaction().commit();
        }
    }

    @Override
    public StudentCourse rateCourse(StudentCourse studentCourse) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(studentCourse);
            session.getTransaction().commit();
        }
        return studentCourse;
    }

    public double getStudentCourseAverageGrade(int courseId, int studentId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> query = session
                    .createQuery("select avg(sl.grade) from StudentLecture sl join sl.lecture sll " +
                            "where sll.course.id = :courseId and sl.student.id = :studentId", Double.class);

            query.setParameter("courseId", courseId);
            query.setParameter("studentId", studentId);

            return query.list().get(0);
        }
    }

    @Override
    public boolean isUserEnrolled(int courseId, User lu) {
        try (Session session = sessionFactory.openSession()) {
            Query<StudentCourse> query = session
                    .createQuery("from StudentCourse s where s.student.id = :studentId AND s.course.id = :courseId", StudentCourse.class);

            query.setParameter("studentId", lu.getId());
            query.setParameter("courseId", courseId);

            List<StudentCourse> studentCourse = query.list();

            return studentCourse.size() != 0;
        }
    }

    @Override
    public boolean isCourseRated(int courseId, User lu) {
        try (Session session = sessionFactory.openSession()) {
            Query<StudentCourse> query = session
                    .createQuery("from StudentCourse s where s.student.id = :studentId AND s.course.id = :courseId", StudentCourse.class);

            query.setParameter("studentId", lu.getId());
            query.setParameter("courseId", courseId);

            List<StudentCourse> studentCourse = query.list();

            if (studentCourse.size() == 0) {
                return false;
            }

            return studentCourse.get(0).getCourseRating() != 0;
        }
    }
}