package com.telerikacademy.web.chefmeup.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class LectureSearchDto {

    private String title;
    private String topic;
    private String sort;
    private String status;
    private String course;

}
