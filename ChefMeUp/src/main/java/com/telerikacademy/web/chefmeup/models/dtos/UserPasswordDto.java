package com.telerikacademy.web.chefmeup.models.dtos;

import com.github.ankurpathak.password.bean.constraints.ContainDigit;
import com.github.ankurpathak.password.bean.constraints.ContainSpecial;
import com.github.ankurpathak.password.bean.constraints.ContainUppercase;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class UserPasswordDto {

    @NotBlank
    @Size(min = 8, message = "The password must be at least 8 symbols.")
    @ContainDigit(message = "The password must contain at least 1 digit.")
    @ContainUppercase(message = "The password must contain at least 1 uppercase letter.")
    @ContainSpecial(message = "The password must contain at least 1 special symbol.")
    private String password;

    @NotBlank
    private String confirmPassword;

    private String oldPassword;

}
