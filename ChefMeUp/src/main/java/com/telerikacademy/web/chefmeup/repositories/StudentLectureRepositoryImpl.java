package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.exeptions.EntityNotFoundException;
import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.StudentLecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentLectureRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Objects;

@Repository
public class StudentLectureRepositoryImpl implements StudentLectureRepository {

    private static final String STORE_FILE_ERROR_MSG = "Could not store file. Please try again!";

    private final SessionFactory sessionFactory;

    @Autowired
    public StudentLectureRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void create(StudentLecture studentLecture) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(studentLecture);
            session.getTransaction().commit();
        }
    }

    @Override
    public void submitAssignment(MultipartFile file, Lecture lecture, User lu, String name, Path assignments) {
        try {
            Files.copy(file.getInputStream(), assignments, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new NotAllowedOperationException(STORE_FILE_ERROR_MSG);
        }
        StudentLecture studentLecture = getByStudentAndLecture(lu.getId(), lecture.getId());
        studentLecture.setAssignment(name);
        update(studentLecture);
    }

    @Override
    public List<StudentLecture> getTeachersLectureAssignments(User loggedUser) {
        try (Session session = sessionFactory.openSession()) {
            Query<StudentLecture> query = session
                    .createQuery("from StudentLecture where lecture.course.creator.id = :creatorId and grade = 0 and assignment != null", StudentLecture.class);
            query.setParameter("creatorId", loggedUser.getId());

            return query.list();
        }
    }

    @Override
    public StudentLecture getById(int assignmentId) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(StudentLecture.class, assignmentId);
        }
    }

    @Override
    public List<StudentLecture> getAllStudentsLectures(User loggedUser, int courseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<StudentLecture> query = session
                    .createQuery("from StudentLecture where student.id = :studentId and lecture.course.id = :courseId", StudentLecture.class);
            query.setParameter("studentId", loggedUser.getId());
            query.setParameter("courseId", courseId);

            return query.list();
        }
    }

    @Override
    public StudentLecture getByStudentAndLecture(int studentId, int lectureId) {
        try (Session session = sessionFactory.openSession()) {
            Query<StudentLecture> query = session
                    .createQuery("from StudentLecture where student.id = :studentId and lecture.id = :lectureId", StudentLecture.class);
            query.setParameter("studentId", studentId);
            query.setParameter("lectureId", lectureId);

            List<StudentLecture> studentLecture = query.list();
            if (studentLecture.size() == 0) {
                throw new EntityNotFoundException("Student with such lecture does not exist.");
            }

            return studentLecture.get(0);
        }
    }

    @Override
    public void submitAssignmentToLecture(StudentLecture studentLecture, MultipartFile file) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        Path resourceDirectory = Paths.get("src", "main", "resources", "assignments", "students", fileName);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path assignments = Paths.get(absolutePath);

        try {
            Files.copy(file.getInputStream(), assignments, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new EntityNotFoundException("opravi greshakat");
        }

        studentLecture.setAssignment(fileName);
        update(studentLecture);
    }

    @Override
    public void update(StudentLecture studentLecture) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(studentLecture);
            session.getTransaction().commit();
        }
    }

}
