package com.telerikacademy.web.chefmeup.models.mappers;

import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.dtos.LectureDto;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.LectureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LectureMapper {

    private final LectureRepository lectureRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public LectureMapper(LectureRepository lectureRepository,
                         CourseRepository courseRepository) {
        this.lectureRepository = lectureRepository;
        this.courseRepository = courseRepository;
    }

    public Lecture lectureFromDto(LectureDto lectureDto) {
        if (lectureDto.getVideo().contains("?v=")) {
            embedLink(lectureDto);
        }
        Lecture lecture = new Lecture();
        lectureDtoToObject(lectureDto, lecture);

        return lecture;
    }

    public Lecture lectureFromDto(LectureDto lectureDto, int id) {
        if (lectureDto.getVideo().contains("?v=")) {
            embedLink(lectureDto);
        }
        Lecture lecture = lectureRepository.getById(id);
        lectureDtoToObject(lectureDto, lecture);

        return lecture;
    }

    private void lectureDtoToObject(LectureDto lectureDto, Lecture lecture) {
        lecture.setTitle(lectureDto.getTitle());
        lecture.setDescription(lectureDto.getDescription());
        if (!lectureDto.getAssigment().isEmpty()) {
            lecture.setAssigment(lectureDto.getAssigment());
        }
        lecture.setVideo(lectureDto.getVideo());
        lecture.setCourse(courseRepository.getById(lectureDto.getCourseId()));
    }

    public LectureDto lectureDtoFromObject(Lecture lecture) {
        LectureDto lectureDto = new LectureDto();
        lectureDto.setVideo(lecture.getVideo());
        lectureDto.setAssigment(lecture.getAssigment());
        lectureDto.setDescription(lecture.getDescription());
        lectureDto.setTitle(lecture.getTitle());
        lectureDto.setCourseId(lecture.getCourse().getId());
        return lectureDto;
    }

    private void embedLink(LectureDto lectureDto) {
        String url = lectureDto.getVideo();
        String id = url.substring(url.lastIndexOf("?v=") + 3);
        String embedLink = "http://www.youtube.com/embed/" + id;
        lectureDto.setVideo(embedLink);
    }

}
