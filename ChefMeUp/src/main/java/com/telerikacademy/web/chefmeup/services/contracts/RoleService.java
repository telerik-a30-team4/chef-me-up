package com.telerikacademy.web.chefmeup.services.contracts;

import com.telerikacademy.web.chefmeup.models.Role;

public interface RoleService {

    Role getById(int id);

}
