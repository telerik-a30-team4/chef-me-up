package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.StudentCourse;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.CourseSearchDto;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Repository
public class CourseRepositoryImpl extends BaseModifyRepositoryImpl<Course> implements CourseRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CourseRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Course.class);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public boolean doesCourseTitleExist(String title, int courseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("from Course where title = :title and id != :courseId", Course.class);

            query.setParameter("title", title);
            query.setParameter("courseId", courseId);

            return !query.list().isEmpty();
        }
    }

    @Override
    public int getCourseStudentsCount(int courseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<StudentCourse> query = session.createQuery("from StudentCourse where course.id = :courseId", StudentCourse.class);

            query.setParameter("courseId", courseId);

            return query.list().size();
        }
    }

    @Override
    public double getCourseAverageRating(int courseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> query = session.createQuery("select avg(sc.courseRating) from StudentCourse sc where sc.course.id = :courseId and sc.courseRating != 0", Double.class);

            query.setParameter("courseId", courseId);

            if (query.list().get(0) == null) {
                return 0;
            }

            return query.list().get(0);
        }
    }

    @Override
    public void uploadCoursePicture(MultipartFile file, Course newCourse, String name, Path assignments) {
        try {
            Files.copy(file.getInputStream(), assignments, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new NotAllowedOperationException("Could not store file. Please try again!");
        }
        newCourse.setPicture(name);
        update(newCourse);
    }

    @Override
    public List<Course> getAllTeacherDraftCourses(User lu) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("from Course where creator.id = :creatorId and visible = false", Course.class);
            query.setParameter("creatorId", lu.getId());
            return query.list();
        }
    }

    @Override
    public List<Course> getAllTeacherCourses(User lu) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("from Course where creator.id = :creatorId", Course.class);
            query.setParameter("creatorId", lu.getId());
            return query.list();
        }
    }

    @Override
    public List<Course> getAllUsersEnrolledCourses(User loggedUser) {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("select sc.course from StudentCourse sc where sc.student.id = :userId", Course.class);
            query.setParameter("userId", loggedUser.getId());
            return query.list();
        }
    }

    @Override
    public List<Course> search(CourseSearchDto courseSearchDto) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder();
            queryString.append("from Course where 1=1");

            if (courseSearchDto.getTitle() != null && !courseSearchDto.getTitle().isEmpty()) {
                queryString.append(" and title like ").append("'%")
                        .append(courseSearchDto.getTitle()).append("%'");
            }

            if (courseSearchDto.getTopic() != null && !courseSearchDto.getTopic().isEmpty())
                queryString.append(" and topic.id = ")
                        .append(courseSearchDto.getTopic());

            if (courseSearchDto.getCreator() != null && !courseSearchDto.getCreator().isEmpty())
                queryString.append(" and concat(creator.firstName, ' ', creator.lastName) like ").append("'%")
                        .append(courseSearchDto.getCreator()).append("%'");

            if (courseSearchDto.getStatus() != null && !courseSearchDto.getStatus().isEmpty())
                queryString.append(" and visible = ").append(courseSearchDto.getStatus());

            if (courseSearchDto.isOnline())
                queryString.append(" and visible = true");

            if (!courseSearchDto.getSort().isEmpty())
                queryString.append(" order by ")
                        .append(courseSearchDto.getSort());

            Query<Course> query = session.createQuery(queryString.toString(), Course.class);

            return query.list();
        }
    }

    @Override
    public List<Course> getAllOnlineCourses() {
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("from Course where visible = true ", Course.class);
            return query.list();
        }
    }

}
