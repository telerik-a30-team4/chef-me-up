package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.models.Country;
import com.telerikacademy.web.chefmeup.repositories.contracts.CountryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CountryRepositoryImpl extends BaseGetRepositoryImpl<Country> implements CountryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Country.class);
        this.sessionFactory = sessionFactory;
    }

}
