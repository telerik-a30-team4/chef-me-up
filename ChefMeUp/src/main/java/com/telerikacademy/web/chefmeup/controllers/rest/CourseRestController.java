package com.telerikacademy.web.chefmeup.controllers.rest;

import com.telerikacademy.web.chefmeup.config.security.AppUserDetails;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.StudentCourse;
import com.telerikacademy.web.chefmeup.models.dtos.CourseDto;
import com.telerikacademy.web.chefmeup.models.dtos.RateStudentCourseDto;
import com.telerikacademy.web.chefmeup.models.mappers.CourseMapper;
import com.telerikacademy.web.chefmeup.services.contracts.CourseService;
import com.telerikacademy.web.chefmeup.services.contracts.StudentCourseService;
import com.telerikacademy.web.chefmeup.services.contracts.StudentLectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/courses")
public class CourseRestController {

    private final CourseService courseService;
    private final CourseMapper courseMapper;
    private final StudentCourseService studentCourseService;
    private final StudentLectureService studentLectureService;

    @Autowired
    public CourseRestController(CourseService courseService,
                                CourseMapper courseMapper,
                                StudentCourseService studentCourseService,
                                StudentLectureService studentLectureService) {
        this.courseService = courseService;
        this.courseMapper = courseMapper;
        this.studentCourseService = studentCourseService;
        this.studentLectureService = studentLectureService;
    }

    @GetMapping
    public List<Course> getAll() {
        return courseService.getAll();
    }

    @GetMapping("/{courseId}")
    public Course getById(@PathVariable int courseId) {
        return courseService.getById(courseId);
    }

    @Secured("ROLE_TEACHER")
    @PostMapping
    public Course create(@RequestBody @Valid CourseDto courseDto,
                         @AuthenticationPrincipal AppUserDetails loggedUser) {
        Course course = courseMapper.courseFromDto(courseDto);
        courseService.create(course, loggedUser.getUser());

        return course;
    }

    @Secured("ROLE_STUDENT")
    @PostMapping("/{courseId}/enroll")
    public void enroll(@PathVariable int courseId,
                       @AuthenticationPrincipal AppUserDetails loggedUser) {
        studentCourseService.enroll(courseId, loggedUser.getUser());
        studentLectureService.assignLecturesToUser(courseId, loggedUser.getUser());
    }

    @Secured("ROLE_STUDENT")
    @PutMapping("/{courseId}/rate")
    public StudentCourse rateCourse(@PathVariable int courseId,
                              @RequestBody @Valid RateStudentCourseDto rateStudentCourseDto,
                              @AuthenticationPrincipal AppUserDetails loggedUser) {
        int rate = rateStudentCourseDto.getCourseRating();
        return studentCourseService.rateCourse(courseId, loggedUser.getUser(), rate);
    }

    @Secured("ROLE_TEACHER")
    @PutMapping("/{courseId}")
    public Course update(@PathVariable int courseId,
                         @RequestBody @Valid CourseDto courseDto,
                         @AuthenticationPrincipal AppUserDetails loggedUser) {
        Course course = courseMapper.courseFromDto(courseDto, courseId);
        courseService.update(course, loggedUser.getUser());

        return course;
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{courseId}")
    public void delete(@PathVariable int courseId) {
        courseService.delete(courseId);
    }

}
