package com.telerikacademy.web.chefmeup.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class CourseSearchDto {

    private String title;
    private String creator;
    private String topic;
    private String sort;
    private String startDate;
    private String expireDate;
    private String status;
    private boolean isOnline;

}
