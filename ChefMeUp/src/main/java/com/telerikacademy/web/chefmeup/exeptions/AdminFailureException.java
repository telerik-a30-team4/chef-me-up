package com.telerikacademy.web.chefmeup.exeptions;

public class AdminFailureException extends RuntimeException {
    public AdminFailureException(String message) {
        super(message);
    }
}
