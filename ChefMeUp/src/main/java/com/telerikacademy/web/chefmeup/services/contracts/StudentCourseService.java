package com.telerikacademy.web.chefmeup.services.contracts;

import com.telerikacademy.web.chefmeup.models.StudentCourse;
import com.telerikacademy.web.chefmeup.models.User;

import java.util.List;

public interface StudentCourseService {

    List<StudentCourse> getAll();

    StudentCourse getStudentCourse(int studentId, int courseId);

    void enroll(int courseId, User loggedUser);

    StudentCourse rateCourse(int courseId, User loggedUser, int rate);

    boolean isUserEnrolled(int courseId, User lu);

    boolean isCourseRated(int courseId, User lu);
}
