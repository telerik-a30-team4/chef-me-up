package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.models.Country;
import com.telerikacademy.web.chefmeup.repositories.contracts.CountryRepository;
import com.telerikacademy.web.chefmeup.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public List<Country> getAll() {
       return countryRepository.getAll();
    }

    public Country getById(int countryId) {
        return countryRepository.getById(countryId);
    }


}
