package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.models.Topic;
import com.telerikacademy.web.chefmeup.repositories.contracts.TopicRepository;
import com.telerikacademy.web.chefmeup.services.contracts.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {

    private final TopicRepository topicRepository;

    @Autowired
    public TopicServiceImpl(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    public List<Topic> getAll() {
        return topicRepository.getAll();
    }
}
