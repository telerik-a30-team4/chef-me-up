package com.telerikacademy.web.chefmeup.services.contracts;

import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.TeacherRequestDto;
import com.telerikacademy.web.chefmeup.models.params.UserSearchParams;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {
    List<User> getAll(User loggedUser);

    User getByEmail(String email);

    User getByPhone(String phone);

    void create(User user);

    User getById(int userId, User loggedUser);

    void update(User userToUpdate, User loggedUser);

    void delete(int userId, User user);

    List<User> search(UserSearchParams userSearchParams, User loggedUser);

    void uploadUserProfilePicture(MultipartFile file, User user);

    List<User> getAllStudentsWithTeacherRequest();

    void teacherRequestApproval(TeacherRequestDto teacherRequestDto, int userId);
}
