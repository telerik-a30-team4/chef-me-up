package com.telerikacademy.web.chefmeup.repositories.contracts;

import com.telerikacademy.web.chefmeup.models.Topic;
import com.telerikacademy.web.chefmeup.repositories.BaseGetRepositoryImpl;

public interface TopicRepository extends BaseGetRepository<Topic> {

}
