package com.telerikacademy.web.chefmeup.controllers.mvc;

import com.telerikacademy.web.chefmeup.config.security.AppUserDetails;
import com.telerikacademy.web.chefmeup.exeptions.DuplicateEntityException;
import com.telerikacademy.web.chefmeup.models.Country;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.RegisterUserDto;
import com.telerikacademy.web.chefmeup.models.dtos.TeacherRequestDto;
import com.telerikacademy.web.chefmeup.models.dtos.UserPasswordDto;
import com.telerikacademy.web.chefmeup.models.mappers.UserMapper;
import com.telerikacademy.web.chefmeup.models.params.UserSearchParams;
import com.telerikacademy.web.chefmeup.services.contracts.CountryService;
import com.telerikacademy.web.chefmeup.services.contracts.StudentLectureService;
import com.telerikacademy.web.chefmeup.services.contracts.UserService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final StudentLectureService studentLectureService;
    private final CountryService countryService;

    public UserMvcController(UserService userService,
                             UserMapper userMapper,
                             StudentLectureService studentLectureService,
                             CountryService countryService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.studentLectureService = studentLectureService;
        this.countryService = countryService;
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAll();
    }

    @Secured("ROLE_TEACHER")
    @GetMapping
    public String showAllUsers(Model model,
                               @AuthenticationPrincipal AppUserDetails userDetails) {
        User loggedUser = userDetails.getUser();
        model.addAttribute("users", userService.getAll(loggedUser));
        model.addAttribute("userSearch", new UserSearchParams());
        return "users";
    }

    @Secured("ROLE_TEACHER")
    @PostMapping("/search")
    public String search(@ModelAttribute("userSearch") UserSearchParams userSearchDto,
                         Model model,
                         @AuthenticationPrincipal AppUserDetails userDetails) {
        User loggedUser = userDetails.getUser();
        List<User> searchUsers = userService.search(userSearchDto, loggedUser);

        model.addAttribute("users", searchUsers);

        return "users";
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/{userId}")
    public String showUserProfile(@PathVariable int userId,
                                  Model model,
                                  @AuthenticationPrincipal AppUserDetails userDetails) {

        User loggedUser = userDetails.getUser();
        User userToShow = userService.getById(userId, loggedUser);

        RegisterUserDto registerUserDto = userMapper.registerUserDtoFromObject(userToShow);

        model.addAttribute("highestRole", userToShow.getHighestRole());
        model.addAttribute("user", registerUserDto);

        return "user-profile";
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/profile")
    public String showMyProfile(@AuthenticationPrincipal AppUserDetails userDetails) {

        User loggedUser = userDetails.getUser();

        return "redirect:/users/" + loggedUser.getId();
    }

    @Secured("ROLE_STUDENT")
    @PostMapping("/{userId}")
    public String updateUser(@PathVariable int userId,
                             @Valid @ModelAttribute("user") RegisterUserDto register,
                             BindingResult bindingResult,
                             @AuthenticationPrincipal AppUserDetails userDetails,
                             @RequestParam("file") MultipartFile file) {

        if (bindingResult.hasErrors()) {
            return "user-profile";
        }

        User loggedUser = userDetails.getUser();
        User userToUpdate = userMapper.userFromDto(register, userId);

        userService.update(userToUpdate, loggedUser);

        if (!file.isEmpty()) {
            userService.uploadUserProfilePicture(file, userToUpdate);
        }

        return "redirect:/users/" + userId;
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/{userId}/password")
    public String showUpdateUserPasswordPage(@PathVariable int userId,
                                             Model model) {
        model.addAttribute("password", new UserPasswordDto());

        return "user-changePassword";
    }

    @Secured("ROLE_STUDENT")
    @PostMapping("/{userId}/password")
    public String updateUserPassword(@PathVariable int userId,
                                     @Valid @ModelAttribute("password") UserPasswordDto userPasswordDto,
                                     BindingResult bindingResult,
                                     @AuthenticationPrincipal AppUserDetails userDetails) {

        if (bindingResult.hasErrors()) {
            return "user-changePassword";
        }

        User loggedUser = userDetails.getUser();
        User userToUpdate = userService.getById(userId, loggedUser);

        if (userPasswordDto.getOldPassword() == null || !userPasswordDto.getOldPassword().equals(userToUpdate.getPassword())) {
            bindingResult.rejectValue("oldPassword", "password_error", "Does not match to your current password");
            return "user-changePassword";
        }

        if (!userPasswordDto.getPassword().equals(userPasswordDto.getConfirmPassword())) {
            bindingResult.rejectValue("confirmPassword", "password_error", "Confirm password does not match the new password");
            return "user-changePassword";
        }

        userMapper.userChangePassword(userToUpdate, userPasswordDto);
        userService.update(userToUpdate, loggedUser);

        return "redirect:/users/" + userId;
    }


    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterUserDto());
        model.addAttribute("password", new UserPasswordDto());
        return "user-register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute("register") RegisterUserDto registerUserDto,
                               BindingResult bindingResultRegister,
                               @Valid @ModelAttribute("password") UserPasswordDto userPasswordDto,
                               BindingResult bindingResultPassword,
                               @AuthenticationPrincipal AppUserDetails loggedUser,
                               @RequestParam("file") MultipartFile file) {
        if (bindingResultRegister.hasErrors() || bindingResultPassword.hasErrors()) {
            return "user-register";
        }

        if (!userPasswordDto.getPassword().equals(userPasswordDto.getConfirmPassword())) {
            bindingResultPassword.rejectValue("confirmPassword", "password_error", "Passwords do not match");
            return "user-register";
        }

        User newUser = userMapper.userFromDto(registerUserDto);
        userMapper.userChangePassword(newUser, userPasswordDto);

        try {
            userService.create(newUser);
        } catch (DuplicateEntityException e) {
            bindingResultRegister.rejectValue("email", "error", "Email or Phone are already in use");
            return "user-register";
        }

        if (!file.isEmpty()) {
            userService.uploadUserProfilePicture(file, newUser);
        }

        return "redirect:/login";
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/requests")
    public String showUsersWithTeacherRequest(Model model,
                               @AuthenticationPrincipal AppUserDetails userDetails) {
        User loggedUser = userDetails.getUser();
        List<User> studentsWithTeacherRequest = userService.getAllStudentsWithTeacherRequest();

        model.addAttribute("users", studentsWithTeacherRequest);
        model.addAttribute("request", new TeacherRequestDto());

        return "users-teacher-requests";
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/{userId}/request")
    public String teacherRequestApproval(@PathVariable int userId,
                                         Model model,
                                         @ModelAttribute("request") TeacherRequestDto teacherRequestDto) {
        userService.teacherRequestApproval(teacherRequestDto, userId);

        List<User> studentsWithTeacherRequest = userService.getAllStudentsWithTeacherRequest();
        model.addAttribute("users", studentsWithTeacherRequest);

        return "users-teacher-requests";
    }


}
