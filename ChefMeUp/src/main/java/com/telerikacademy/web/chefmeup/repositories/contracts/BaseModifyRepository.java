package com.telerikacademy.web.chefmeup.repositories.contracts;

public interface BaseModifyRepository<T> extends BaseGetRepository<T> {

    void create(T entity);

    void update(T entity);

    void delete(int id);

}
