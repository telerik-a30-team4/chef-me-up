package com.telerikacademy.web.chefmeup.controllers.mvc;

import com.telerikacademy.web.chefmeup.config.security.AppUserDetails;
import com.telerikacademy.web.chefmeup.models.*;
import com.telerikacademy.web.chefmeup.models.dtos.GradeStudentLectureDto;
import com.telerikacademy.web.chefmeup.models.dtos.LectureDto;
import com.telerikacademy.web.chefmeup.models.dtos.LectureSearchDto;
import com.telerikacademy.web.chefmeup.models.mappers.LectureMapper;
import com.telerikacademy.web.chefmeup.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
@RequestMapping("/lectures")
public class LectureMvcController {

    private final LectureService lectureService;
    private final CourseService courseService;
    private final LectureMapper lectureMapper;
    private final StudentLectureService studentLectureService;
    private final TopicService topicService;

    @Autowired
    public LectureMvcController(LectureService lectureService,
                                CourseService courseService,
                                LectureMapper lectureMapper,
                                StudentLectureService studentLectureService,
                                TopicService topicService) {
        this.lectureService = lectureService;
        this.courseService = courseService;
        this.lectureMapper = lectureMapper;
        this.studentLectureService = studentLectureService;
        this.topicService = topicService;
    }

    @ModelAttribute("courses")
    public List<Course> populateAllCourses() {
        return courseService.getAll();
    }

    @ModelAttribute("topics")
    public List<Topic> populateTopics() {
        return topicService.getAll();
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/{lectureId}")
    public String showSingleLecture(@PathVariable int lectureId,
                                    Model model,
                                    @AuthenticationPrincipal AppUserDetails user) {
        User loggedUser = user.getUser();
        Lecture lecture = lectureService.getById(lectureId, loggedUser);
        model.addAttribute("lecture", lecture);

        return "lecture";
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/new")
    public String showNewLecturePage(Model model,
                                     @AuthenticationPrincipal AppUserDetails userDetails) {
        User loggedUser = userDetails.getUser();
        model.addAttribute("lecture", new LectureDto());
        model.addAttribute("draftCourses", courseService.getAllTeacherDraftCourses(loggedUser));
        return "lecture-new";
    }

    @Secured("ROLE_TEACHER")
    @PostMapping("/new")
    public String createLecture(@Valid @ModelAttribute("lecture") LectureDto lectureDto,
                                BindingResult errors,
                                @AuthenticationPrincipal AppUserDetails user,
                                @RequestParam("file") MultipartFile file,
                                @RequestParam("picture") MultipartFile picture) {
        if (errors.hasErrors()) {
            return "lecture-new";
        }
        User loggedUser = user.getUser();
        Lecture newLecture = lectureMapper.lectureFromDto(lectureDto);

        lectureService.create(newLecture, loggedUser);
        lectureService.uploadAssignment(file, newLecture, loggedUser);
        lectureService.uploadLecturePicture(picture, newLecture);

        return "redirect:/lectures/" + newLecture.getId();
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/{lectureId}/download/assignment")
    public void downloadAssignment(@PathVariable int lectureId,
                                   @AuthenticationPrincipal AppUserDetails user,
                                   HttpServletResponse response) throws IOException {
        User loggedUser = user.getUser();
        Lecture lecture = lectureService.getById(lectureId, loggedUser);
        String fileName = lecture.getAssigment();

        Path resourceDirectory = Paths.get("media", "lectureassignments", fileName);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();

        downloadFile(response, absolutePath);
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/{lectureId}/student/{studentId}/assignment")
    public void downloadStudentAssignment(@PathVariable int studentId,
                                          @PathVariable int lectureId,
                                          HttpServletResponse response) throws IOException {
        StudentLecture studentLecture = studentLectureService.getByStudentAndLecture(studentId, lectureId);
        String fileName = studentLecture.getAssignment();

        Path resourceDirectory = Paths.get("media", "studentsassignments", fileName);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();

        downloadFile(response, absolutePath);
    }

    @Secured("ROLE_TEACHER")
    @PostMapping("/{lectureId}/upload/assignment")
    public String uploadAssignment(@RequestParam("file") MultipartFile file,
                                   @PathVariable int lectureId,
                                   @AuthenticationPrincipal AppUserDetails user,
                                   Model model,
                                   RedirectAttributes redirectAttributes) {
        User loggedUser = user.getUser();
        Lecture lecture = lectureService.getById(lectureId, loggedUser);
        lectureService.uploadAssignment(file, lecture, loggedUser);

        model.addAttribute("lecture", lecture);
        redirectAttributes.addFlashAttribute("success", "File was uploaded successfully");

        return "redirect:/lectures/" + lectureId;
    }

    @Secured("ROLE_STUDENT")
    @PostMapping("/{lectureId}/submit/assignment")
    public String submitAssignment(@RequestParam("file") MultipartFile file,
                                   @PathVariable int lectureId,
                                   @AuthenticationPrincipal AppUserDetails user,
                                   Model model,
                                   RedirectAttributes redirectAttributes) {
        User loggedUser = user.getUser();
        Lecture lecture = lectureService.getById(lectureId, loggedUser);
        studentLectureService.submitAssignment(file, lecture, loggedUser);

        model.addAttribute("lecture", lecture);
        redirectAttributes.addFlashAttribute("success", "File was uploaded successfully");

        return "redirect:/lectures/" + lectureId;
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/{lectureId}/update")
    public String showUpdateLecturePage(@PathVariable int lectureId,
                                        Model model,
                                        @AuthenticationPrincipal AppUserDetails user) {
        User loggedUser = user.getUser();
        Lecture lecture = lectureService.getById(lectureId, loggedUser);
        LectureDto lectureDto = lectureMapper.lectureDtoFromObject(lecture);
        model.addAttribute("lecture", lectureDto);
        return "lecture-update";
    }

    @Secured("ROLE_TEACHER")
    @PostMapping("/{lectureId}/update")
    public String updateLecture(@PathVariable int lectureId,
                                @Valid @ModelAttribute("lecture") LectureDto lectureDto,
                                BindingResult errors,
                                @AuthenticationPrincipal AppUserDetails user,
                                @RequestParam("file") MultipartFile file,
                                @RequestParam("picture") MultipartFile picture) {
        if (errors.hasErrors()) {
            return "lecture-update";
        }
        User loggedUser = user.getUser();
        Lecture newLecture = lectureMapper.lectureFromDto(lectureDto, lectureId);
        lectureService.update(newLecture, loggedUser);
        if (!file.isEmpty()) {
            lectureService.uploadAssignment(file, newLecture, loggedUser);
        }
        if (!picture.isEmpty()) {
            lectureService.uploadLecturePicture(picture, newLecture);
        }
        return "redirect:/lectures/" + lectureId;
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/students/assignments")
    public String getTeachersLectureAssignments(Model model,
                                                @AuthenticationPrincipal AppUserDetails user) {
        User loggedUser = user.getUser();
        List<StudentLecture> assignments = studentLectureService.getTeachersLectureAssignments(loggedUser);
        model.addAttribute("gradeAssignment", new GradeStudentLectureDto());
        model.addAttribute("assignments", assignments);

        return "assignments";
    }

    @Secured("ROLE_TEACHER")
    @PostMapping("/students/assignments/{assignmentId}/grade")
    public String gradeAssignment(@PathVariable int assignmentId,
                                  @Valid @ModelAttribute("gradeAssignment") GradeStudentLectureDto gradeStudentLectureDto,
                                  BindingResult errors,
                                  @AuthenticationPrincipal AppUserDetails user) {
        if (errors.hasErrors()) {
            return "assignments";
        }
        User loggedUser = user.getUser();
        studentLectureService.gradeAssignment(loggedUser, assignmentId, gradeStudentLectureDto.getGrade());

        return "redirect:/lectures/students/assignments";
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/dashboard")
    public String getTeachersLectures(@AuthenticationPrincipal AppUserDetails user,
                                      Model model) {
        User loggedUser = user.getUser();
        List<Lecture> teacherLectures = lectureService.getAllTeacherLectures(loggedUser);
        model.addAttribute("lectures", teacherLectures);
        model.addAttribute("lectureSearchDto", new LectureSearchDto());

        return "lecture-dashboard";
    }

    @PostMapping("/dashboard/search")
    public String searchLectureDashboard(@ModelAttribute LectureSearchDto lectureSearchDto,
                                         Model model) {
        List<Lecture> searchLectures = lectureService.search(lectureSearchDto);

        model.addAttribute("lectures", searchLectures);

        return "lecture-dashboard";
    }

    private void downloadFile(HttpServletResponse response, String absolutePath) throws IOException {

        File file = new File(absolutePath);

        if (file.exists()) {
            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }

            response.setContentType(mimeType);

            //Preview
//            response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

            //Download
            response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));

            response.setContentLength((int) file.length());

            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

            FileCopyUtils.copy(inputStream, response.getOutputStream());
        }
    }
}
