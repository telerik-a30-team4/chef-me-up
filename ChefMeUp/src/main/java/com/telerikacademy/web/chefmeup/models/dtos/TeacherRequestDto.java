package com.telerikacademy.web.chefmeup.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TeacherRequestDto {

    private boolean isRequestApproved;

}
