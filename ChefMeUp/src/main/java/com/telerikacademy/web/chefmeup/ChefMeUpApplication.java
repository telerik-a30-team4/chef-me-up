package com.telerikacademy.web.chefmeup;

import com.telerikacademy.web.chefmeup.repositories.contracts.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class ChefMeUpApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChefMeUpApplication.class, args);
    }

}
