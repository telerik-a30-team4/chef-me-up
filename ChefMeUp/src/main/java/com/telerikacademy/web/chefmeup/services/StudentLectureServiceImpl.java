package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.StudentLecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.LectureRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentLectureRepository;
import com.telerikacademy.web.chefmeup.services.contracts.StudentLectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

@Service
public class StudentLectureServiceImpl implements StudentLectureService {

    private static final String UPLOAD_ASSIGNMENT_ERROR_MSG = "You can upload assignments only to your profile.";

    private final StudentLectureRepository studentLectureRepository;
    private final LectureRepository lectureRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public StudentLectureServiceImpl(StudentLectureRepository studentLectureRepository,
                                     LectureRepository lectureRepository,
                                     CourseRepository courseRepository) {
        this.studentLectureRepository = studentLectureRepository;
        this.lectureRepository = lectureRepository;
        this.courseRepository = courseRepository;
    }

    public StudentLecture getByStudentAndLecture(int studentId, int lectureId) {
        return studentLectureRepository.getByStudentAndLecture(studentId, lectureId);
    }

    @Override
    public void submitAssignmentToLecture(StudentLecture studentLecture, MultipartFile file, User loggedUser) {

        if (studentLecture.getStudent().getId() != loggedUser.getId()) {
            throw new UnauthorizedOperationException(UPLOAD_ASSIGNMENT_ERROR_MSG);
        }

        studentLectureRepository.submitAssignmentToLecture(studentLecture, file);
    }

    @Override
    public void assignLecturesToUser(int courseId, User loggedUser) {
        courseRepository.getById(courseId);

        List<Lecture> lectures = lectureRepository.getAllCourseLectures(courseId);

        for (Lecture lecture : lectures) {
            StudentLecture studentLecture = new StudentLecture();
            studentLecture.setStudent(loggedUser);
            studentLecture.setLecture(lecture);
            studentLectureRepository.create(studentLecture);
        }
    }

    @Override
    public void submitAssignment(MultipartFile file, Lecture lecture, User loggedUser) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = lecture.getCourse().getTitle() + "-" +
                lecture.getTitle() + "-" +
                loggedUser.getEmail() + "." +
                fileName.substring(fileName.lastIndexOf(".") + 1);
        String name = saveAsName.replaceAll(" ", "");
        Path resourceDirectory = Paths.get( "media","studentsassignments", name);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path assignments = Paths.get(absolutePath);

        studentLectureRepository.submitAssignment(file, lecture, loggedUser, name, assignments);
    }

    @Override
    public List<StudentLecture> getTeachersLectureAssignments(User loggedUser) {
        return studentLectureRepository.getTeachersLectureAssignments(loggedUser);
    }

    @Override
    public void gradeAssignment(User loggedUser, int assignmentId, int grade) {
        StudentLecture assignment = studentLectureRepository.getById(assignmentId);
        assignment.setGrade(grade);
        assignment.setCompleted(true);
        studentLectureRepository.update(assignment);
    }

    @Override
    public List<StudentLecture> getAllStudentsLectures(User loggedUser, int courseId) {
        return studentLectureRepository.getAllStudentsLectures(loggedUser, courseId);
    }

}
