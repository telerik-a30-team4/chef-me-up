package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.models.Topic;
import com.telerikacademy.web.chefmeup.repositories.contracts.TopicRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TopicRepositoryImpl extends BaseGetRepositoryImpl<Topic> implements TopicRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TopicRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Topic.class);
        this.sessionFactory = sessionFactory;
    }
}
