package com.telerikacademy.web.chefmeup.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "lectures")
public class Lecture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "lecture_id")
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "video")
    private String video;

    @Column(name = "assigment")
    private String assigment;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;

    @Column(name = "picture")
    private String picture;

}
