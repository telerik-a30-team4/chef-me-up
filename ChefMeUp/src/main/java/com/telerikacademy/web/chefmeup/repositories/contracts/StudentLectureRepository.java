package com.telerikacademy.web.chefmeup.repositories.contracts;

import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.StudentLecture;
import com.telerikacademy.web.chefmeup.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;

public interface StudentLectureRepository {

    StudentLecture getByStudentAndLecture(int studentId, int lectureId);

    void submitAssignmentToLecture(StudentLecture studentLecture, MultipartFile file);

    void update(StudentLecture studentLecture);

    void create(StudentLecture studentLecture);

    void submitAssignment(MultipartFile file, Lecture lecture, User lu, String name, Path assignments);

    List<StudentLecture> getTeachersLectureAssignments(User loggedUser);

    StudentLecture getById(int assignmentId);

    List<StudentLecture> getAllStudentsLectures(User loggedUser, int courseId);

}
