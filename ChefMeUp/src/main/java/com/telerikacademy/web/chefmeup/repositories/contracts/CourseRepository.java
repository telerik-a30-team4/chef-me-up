package com.telerikacademy.web.chefmeup.repositories.contracts;

import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.CourseSearchDto;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;

public interface CourseRepository extends BaseModifyRepository<Course>{

    boolean doesCourseTitleExist(String title, int courseId);

    int getCourseStudentsCount(int courseId);

    double getCourseAverageRating(int courseId);

    void uploadCoursePicture(MultipartFile file, Course newCourse, String name, Path assignments);

    List<Course> getAllTeacherDraftCourses(User lu);

    List<Course> getAllTeacherCourses(User lu);

    List<Course> getAllUsersEnrolledCourses(User loggedUser);

    List<Course> search(CourseSearchDto courseSearchDto);

    List<Course> getAllOnlineCourses();
}
