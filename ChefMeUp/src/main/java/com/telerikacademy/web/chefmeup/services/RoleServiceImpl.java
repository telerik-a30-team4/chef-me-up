package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.models.Role;
import com.telerikacademy.web.chefmeup.repositories.contracts.RoleRepository;
import com.telerikacademy.web.chefmeup.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role getById(int id) {
        return roleRepository.getById(id);
    }
}
