package com.telerikacademy.web.chefmeup.services.contracts;

import com.telerikacademy.web.chefmeup.models.Topic;

import java.util.List;

public interface TopicService {

    List<Topic> getAll();

}
