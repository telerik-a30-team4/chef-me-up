package com.telerikacademy.web.chefmeup.config.security;

import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AppUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public AppUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public AppUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.getByEmail(username, 0);

        if (user == null) {
            throw new UsernameNotFoundException("Wrong username or password");
        }

        return new AppUserDetails(user);
    }

}
