package com.telerikacademy.web.chefmeup.repositories.contracts;

import java.util.List;

public interface BaseGetRepository<T> {

    List<T> getAll();

    T getById(int id);

    T getByField(String fieldName, Object fieldValue);

}
