package com.telerikacademy.web.chefmeup.controllers.rest;

import com.telerikacademy.web.chefmeup.config.security.AppUserDetails;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.StudentLecture;
import com.telerikacademy.web.chefmeup.models.dtos.LectureDto;
import com.telerikacademy.web.chefmeup.models.mappers.LectureMapper;
import com.telerikacademy.web.chefmeup.services.contracts.LectureService;
import com.telerikacademy.web.chefmeup.services.contracts.StudentLectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/api/lectures")
public class LectureRestController {

    private final LectureService lectureService;
    private final LectureMapper lectureMapper;
    private final StudentLectureService studentLectureService;

    @Autowired
    public LectureRestController(LectureService lectureService,
                                 LectureMapper lectureMapper,
                                 StudentLectureService studentLectureService) {
        this.lectureService = lectureService;
        this.lectureMapper = lectureMapper;
        this.studentLectureService = studentLectureService;
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/course/{courseId}")
    public List<Lecture> getAllCourseLectures(@PathVariable int courseId,
                                              @AuthenticationPrincipal AppUserDetails loggedUser) {
        return lectureService.getAllCourseLectures(courseId);
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/{lectureId}")
    public Lecture getById(@PathVariable int lectureId,
                           @AuthenticationPrincipal AppUserDetails loggedUser) {
        return lectureService.getById(lectureId, loggedUser.getUser());
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/{lectureId}/student/{studentId}/assignment")
    public void downloadStudentAssignment(HttpServletResponse response,
                                          @PathVariable int lectureId,
                                          @PathVariable int studentId) throws IOException {
        StudentLecture studentLecture = studentLectureService.getByStudentAndLecture(studentId, lectureId);
        String fileName = studentLecture.getAssignment();

        Path resourceDirectory = Paths.get("src", "main", "resources", "assignments", "students", fileName);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();

        downloadFile(response, absolutePath);
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/{id}/assignment")
    public void downloadAssignment(HttpServletResponse response,
                                   @PathVariable int id,
                                   @AuthenticationPrincipal AppUserDetails loggedUser) throws IOException {
        Lecture lecture = lectureService.getById(id, loggedUser.getUser());
        String fileName = lecture.getAssigment();

        Path resourceDirectory = Paths.get("src", "main", "resources", "assignments", "teachers", fileName);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();

        downloadFile(response, absolutePath);
    }

    @Secured("ROLE_TEACHER")
    @PostMapping()
    public Lecture create(@RequestBody @Valid LectureDto lectureDto,
                          @AuthenticationPrincipal AppUserDetails loggedUser) {
        Lecture lecture = lectureMapper.lectureFromDto(lectureDto);
        lectureService.create(lecture, loggedUser.getUser());
        return lecture;
    }

    @Secured("ROLE_TEACHER")
    @PostMapping("/{lectureId}/assignment")
    public void uploadAssignment(@RequestParam("file") MultipartFile file,
                                 @PathVariable int lectureId,
                                 @AuthenticationPrincipal AppUserDetails loggedUser) {
        Lecture lecture = lectureService.getById(lectureId, loggedUser.getUser());
        lectureService.uploadAssignment(file, lecture, loggedUser.getUser());
    }

    @Secured("ROLE_TEACHER")
    @PutMapping("/{id}")
    public Lecture update(@PathVariable int id,
                          @RequestBody @Valid LectureDto lectureDto,
                          @AuthenticationPrincipal AppUserDetails loggedUser) {
        Lecture lecture = lectureMapper.lectureFromDto(lectureDto, id);
        lectureService.update(lecture, loggedUser.getUser());
        return lecture;
    }

    @Secured("ROLE_TEACHER")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id,
                       @AuthenticationPrincipal AppUserDetails loggedUser) {
        lectureService.delete(id, loggedUser.getUser());
    }

    private void downloadFile(HttpServletResponse response, String absolutePath) throws IOException {
        File file = new File(absolutePath);

        if (file.exists()) {
            String mimeType = URLConnection.guessContentTypeFromName(file.getName());
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }

            response.setContentType(mimeType);

            //Preview
//            response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

            //Download
            response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + file.getName() + "\""));

            response.setContentLength((int) file.length());

            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));

            FileCopyUtils.copy(inputStream, response.getOutputStream());
        }
    }

}
