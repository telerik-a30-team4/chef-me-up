package com.telerikacademy.web.chefmeup.controllers.mvc;

import com.telerikacademy.web.chefmeup.exeptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.io.IOException;


@ControllerAdvice(basePackages = "com.telerikacademy.web.chefmeup.controllers.mvc")
public class ErrorMvcController {

    private static final String UNKNOWN_ERROR = "Unknown error.";

    @ExceptionHandler(value = AccessDeniedException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public String exceptionAccessDenied() {
        return "error-4xx";
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String exceptionEntityNotFoundException(final EntityNotFoundException throwable, final Model model) {
        String errorMessage = throwable.getMessage();
        model.addAttribute("message", errorMessage);
        return "error-4xx";
    }

    @ExceptionHandler(value = {DuplicateEntityException.class})
    @ResponseStatus(code = HttpStatus.CONFLICT)
    public String exceptionDuplicateEntityException(final RuntimeException throwable, final Model model) {
        String errorMessage = (throwable != null && throwable.getMessage() != null ? throwable.getMessage() : UNKNOWN_ERROR);
        model.addAttribute("message", errorMessage);
        return "error-4xx";
    }

    @ExceptionHandler(value = {NotAllowedOperationException.class})
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public String exceptionNotAllowedOperationException(final RuntimeException throwable, final Model model) {
        String errorMessage = (throwable != null && throwable.getMessage() != null ? throwable.getMessage() : UNKNOWN_ERROR);
        model.addAttribute("message", errorMessage);
        return "error-4xx";
    }

    @ExceptionHandler(value = {UnauthorizedOperationException.class})
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public String exceptionUnauthorizedOperationException(final RuntimeException throwable, final Model model) {
        String errorMessage = (throwable != null && throwable.getMessage() != null ? throwable.getMessage() : UNKNOWN_ERROR);
        model.addAttribute("message", errorMessage);
        return "error-4xx";
    }

    @ExceptionHandler(value = {VisibilityNotAllowedOperationException.class})
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public String exceptionVisibilityNotAllowedOperationException(final RuntimeException throwable, final Model model) {
        String errorMessage = (throwable != null && throwable.getMessage() != null ? throwable.getMessage() : UNKNOWN_ERROR);
        model.addAttribute("message", errorMessage);
        return "error-4xx";
    }

    @ExceptionHandler(value = {IOException.class})
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public String exceptionIOException(final RuntimeException throwable, final Model model) {
        String errorMessage = (throwable != null && throwable.getMessage() != null ? throwable.getMessage() : UNKNOWN_ERROR);
        model.addAttribute("message", errorMessage);
        return "error-4xx";
    }
}