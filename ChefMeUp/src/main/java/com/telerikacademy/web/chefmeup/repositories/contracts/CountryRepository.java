package com.telerikacademy.web.chefmeup.repositories.contracts;

import com.telerikacademy.web.chefmeup.models.Country;

public interface CountryRepository extends BaseGetRepository<Country> {
}
