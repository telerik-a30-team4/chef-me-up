package com.telerikacademy.web.chefmeup.repositories.contracts;

import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.params.UserSearchParams;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;

public interface UserRepository extends BaseModifyRepository<User> {
    User getByEmail(String email, int userId);

    List<User> search(UserSearchParams userSearchParams);

    User getByPhone(String phone, int userId);

    void uploadUserProfilePicture(MultipartFile file, User user, Path picPath, String fileName);

    List<User> getAllStudentsWithTeacherRequest();
}
