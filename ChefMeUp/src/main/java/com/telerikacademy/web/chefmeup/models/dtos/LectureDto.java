package com.telerikacademy.web.chefmeup.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class LectureDto {

    @NotBlank(message = "Lecture title can't be empty")
    @Size(min = 5,max = 50)
    private String title;

    @NotBlank(message = "Lecture description can't be empty")
    @Size(min = 50, max = 1000)
    private String description;

    @NotBlank(message = "Lecture must contain video")
    private String video;

    @NotBlank(message = "Lecture must contain assigment")
    private String assigment;

    @Positive
    private int courseId;


}
