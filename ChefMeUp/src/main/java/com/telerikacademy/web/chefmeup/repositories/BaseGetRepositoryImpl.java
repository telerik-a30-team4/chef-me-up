package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.exeptions.EntityNotFoundException;
import com.telerikacademy.web.chefmeup.repositories.contracts.BaseGetRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public abstract class BaseGetRepositoryImpl<T> implements BaseGetRepository<T> {

    private final SessionFactory sessionFactory;
    private final Class<T> clazz;

    public BaseGetRepositoryImpl(SessionFactory sessionFactory, Class<T> clazz) {
        this.sessionFactory = sessionFactory;
        this.clazz = clazz;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            String query = String.format("from %s", clazz.getSimpleName());

            return session.createQuery(query, clazz).getResultList();
        }
    }

    @Override
    public T getByField(String fieldName, Object fieldValue) {
        try (Session session = sessionFactory.openSession()) {
            String query = String.format("from %s where %s = :%s", clazz.getSimpleName(), fieldName, fieldName);

            return session.createQuery(query, clazz)
                    .setParameter(fieldName, fieldValue)
                    .uniqueResultOptional()
                    .orElseThrow(() -> new EntityNotFoundException(clazz.getSimpleName(), fieldName, String.valueOf(fieldValue)));
        }
    }

    @Override
    public T getById(int id) {
        return getByField("id", id);
    }

}
