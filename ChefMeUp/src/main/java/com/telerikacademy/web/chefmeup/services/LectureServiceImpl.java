package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.LectureSearchDto;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.LectureRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentCourseRepository;
import com.telerikacademy.web.chefmeup.services.contracts.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

@Service
public class LectureServiceImpl implements LectureService {

    public static final String NOT_ASSIGNED_TO_COURSE_MSG = "You are not assigned to this course.";
    public static final String NOT_CREATOR_OF_COURSE_MSG = "You are not the creator of the course.";
    private final LectureRepository lectureRepository;
    private final StudentCourseRepository studentCourseRepository;
    private final CourseRepository courseRepository;

    @Autowired
    public LectureServiceImpl(LectureRepository lectureRepository,
                              StudentCourseRepository studentCourseRepository,
                              CourseRepository courseRepository) {
        this.lectureRepository = lectureRepository;
        this.studentCourseRepository = studentCourseRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Lecture> getAllCourseLectures(int courseId) {
        return lectureRepository.getAllCourseLectures(courseId);
    }

    @Override
    public Lecture getById(int lectureId, User loggedUser) {
        Course course = lectureRepository.getCourseByLecture(lectureId);
        if (!loggedUser.isAdmin() &&
                isUserNotCreatorOfTheCourse(course, loggedUser) &&
                isUserNotAssignedToCourse(course, loggedUser)) {
            throw new NotAllowedOperationException(NOT_ASSIGNED_TO_COURSE_MSG);
        }
        return lectureRepository.getById(lectureId);
    }

    @Override
    public void create(Lecture lecture, User loggedUser) {
        throwsIfUserNotCreatorOfCourseOrNotAdmin(lecture.getCourse(), loggedUser);
        lectureRepository.create(lecture);
    }

    @Override
    public void uploadAssignment(MultipartFile file, Lecture lecture, User loggedUser) {
        throwsIfUserNotCreatorOfCourseOrNotAdmin(lecture.getCourse(), loggedUser);

        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = lecture.getCourse().getTitle() + "-" +
                lecture.getTitle() + "-" + "assignment" +
                fileName.substring(fileName.lastIndexOf("."));
        String name = saveAsName.replaceAll(" ", "");
        Path resourceDirectory = Paths.get("media", "lectureassignments", name);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path assignments = Paths.get(absolutePath);

        lectureRepository.uploadAssignment(file, lecture, name, assignments);
    }

    @Override
    public void uploadLecturePicture(MultipartFile file, Lecture newLecture) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = newLecture.getTitle() + "-" + "BACKGROUND" +
                fileName.substring(fileName.lastIndexOf("."));
        String name = saveAsName.replaceAll("\\s+", "");
        Path resourceDirectory = Paths.get( "media","lecturebackground", name);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path assignments = Paths.get(absolutePath);

        lectureRepository.uploadLecturePicture(file, newLecture, name, assignments);
    }

    @Override
    public List<Lecture> getAllTeacherLectures(User lu) {
        if (lu.isAdmin()) {
            return lectureRepository.getAll();
        }
        return lectureRepository.getAllTeacherLectures(lu);
    }

    @Override
    public List<Lecture> search(LectureSearchDto lectureSearchDto) {
        return lectureRepository.search(lectureSearchDto);
    }

    @Override
    public void update(Lecture lecture, User loggedUser) {
        throwsIfUserNotCreatorOfCourseOrNotAdmin(lecture.getCourse(), loggedUser);
        lectureRepository.update(lecture);
    }

    @Override
    public void delete(int lectureId, User loggedUser) {
        Course course = lectureRepository.getCourseByLecture(lectureId);
        throwsIfUserNotCreatorOfCourseOrNotAdmin(course, loggedUser);
        lectureRepository.delete(lectureId);
    }

    private boolean isUserNotAssignedToCourse(Course course, User loggedUser) {
        return studentCourseRepository.getStudentCourse(loggedUser.getId(), course.getId()) == null;
    }

    private boolean isUserNotCreatorOfTheCourse(Course course, User loggedUser) {
        return courseRepository.getById(course.getId()).getCreator().getId() != loggedUser.getId();
    }

    private boolean isUserNotCreatorOfCourseOrNotAdmin(Course course, User loggedUser) {
        return isUserNotCreatorOfTheCourse(course, loggedUser) && !loggedUser.isAdmin();
    }

    private void throwsIfUserNotCreatorOfCourseOrNotAdmin(Course course, User loggedUser) {
        if (isUserNotCreatorOfCourseOrNotAdmin(course, loggedUser)) {
            throw new NotAllowedOperationException(NOT_CREATOR_OF_COURSE_MSG);
        }
    }

}
