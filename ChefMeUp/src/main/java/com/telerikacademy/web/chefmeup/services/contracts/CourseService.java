package com.telerikacademy.web.chefmeup.services.contracts;

import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.CourseSearchDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface CourseService {
    List<Course> getAll();

    Course getById(int courseId);

    void create(Course course, User loggedUser);

    void update(Course course, User loggedUser);

    void delete(int courseId);

    double getCourseAverageRating(int courseId);

    void uploadCoursePicture(MultipartFile file, Course newCourse, User lu);

    List<Course> getAllTeacherDraftCourses(User lu);

    List<Course> getAllTeacherCourses(User lu);

    List<Course> getAllUsersEnrolledCourses(User loggedUser);

    List<Course> search(CourseSearchDto courseSearchDto);

    Map<Integer,String> getAllCourseRatings();

    boolean isCourseStarted(int courseId);

    List<Course> getAllOnlineCourses();

    Map<Integer, String> getUsersCourseGrade(User loggedUser);
}
