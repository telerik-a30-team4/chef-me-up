package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.models.Role;
import com.telerikacademy.web.chefmeup.repositories.contracts.RoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl extends BaseGetRepositoryImpl<Role> implements RoleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Role.class);
        this.sessionFactory = sessionFactory;
    }
}
