package com.telerikacademy.web.chefmeup.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "e_mail")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "password")
    private String password;

    @Column(name = "picture")
    private String picture;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

    @Column(name = "teacher_request")
    private boolean teacherRequest;

    @JsonIgnore
    public boolean isStudent() {
        return this.getRoles().stream()
                .anyMatch(r -> r.getName().equalsIgnoreCase("role_student"));
    }

    @JsonIgnore
    public boolean isTeacher() {
        return this.getRoles().stream()
                .anyMatch(r -> r.getName().equalsIgnoreCase("role_teacher"));
    }

    @JsonIgnore
    public boolean isAdmin() {
        return this.getRoles().stream()
                .anyMatch(r -> r.getName().equalsIgnoreCase("role_admin"));
    }

    @JsonIgnore
    public String getHighestRole() {
        if(isAdmin()) return "Admin";
        if(isTeacher()) return "Teacher";
        else return "Student";
    }
}
