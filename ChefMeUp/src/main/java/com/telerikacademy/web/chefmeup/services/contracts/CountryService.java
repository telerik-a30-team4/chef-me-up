package com.telerikacademy.web.chefmeup.services.contracts;

import com.telerikacademy.web.chefmeup.models.Country;

import java.util.List;

public interface CountryService {

    List<Country> getAll();

    Country getById(int countryId);

}
