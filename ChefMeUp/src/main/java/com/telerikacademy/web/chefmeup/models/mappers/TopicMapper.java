package com.telerikacademy.web.chefmeup.models.mappers;

import com.telerikacademy.web.chefmeup.models.Topic;
import com.telerikacademy.web.chefmeup.models.dtos.TopicDto;
import org.springframework.stereotype.Component;

@Component
public class TopicMapper {

    public Topic topicFromDto(TopicDto topicDto) {
        Topic topic = new Topic();
        topicDtoToObject(topicDto, topic);

        return topic;
    }

    private void topicDtoToObject(TopicDto topicDto, Topic topic) {
        topic.setName(topicDto.getName());
    }

}
