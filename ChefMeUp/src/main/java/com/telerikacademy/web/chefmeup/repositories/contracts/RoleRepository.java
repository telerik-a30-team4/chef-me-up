package com.telerikacademy.web.chefmeup.repositories.contracts;

import com.telerikacademy.web.chefmeup.models.Role;

public interface RoleRepository extends BaseGetRepository<Role>{

}
