package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.LectureSearchDto;
import com.telerikacademy.web.chefmeup.repositories.contracts.LectureRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Repository
public class LectureRepositoryImpl extends BaseModifyRepositoryImpl<Lecture> implements LectureRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public LectureRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Lecture.class);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Course getCourseByLecture(int lectureId) {
        getById(lectureId);
        try (Session session = sessionFactory.openSession()) {
            Query<Course> query = session.createQuery("select l.course from Lecture l where l.id = :lectureId", Course.class);
            query.setParameter("lectureId", lectureId);

            return query.list().get(0);
        }
    }

    @Override
    public void uploadAssignment(MultipartFile file, Lecture lecture, String name, Path assignments) {
        try {
            Files.copy(file.getInputStream(), assignments, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new NotAllowedOperationException("Could not store file. Please try again!");
        }
        lecture.setAssigment(name);
        update(lecture);
    }

    @Override
    public void uploadLecturePicture(MultipartFile file, Lecture newLecture, String name, Path assignments) {
        try {
            Files.copy(file.getInputStream(), assignments, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new NotAllowedOperationException("Could not store file. Please try again!");
        }
        newLecture.setPicture(name);
        update(newLecture);
    }

    @Override
    public List<Lecture> getAllTeacherLectures(User lu) {
        try (Session session = sessionFactory.openSession()) {
            Query<Lecture> query = session.createQuery("from Lecture where course.creator.id = :creatorId", Lecture.class);
            query.setParameter("creatorId", lu.getId());

            return query.list();
        }
    }

    @Override
    public List<Lecture> search(LectureSearchDto lectureSearchDto) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder();
            queryString.append("from Lecture where 1=1");

            if (!lectureSearchDto.getCourse().isEmpty())
                queryString.append(" and course.title like ").append("'%")
                        .append(lectureSearchDto.getCourse()).append("%'");

            if (!lectureSearchDto.getStatus().isEmpty())
                queryString.append(" and course.visible = ").append(lectureSearchDto.getStatus());

            if (!lectureSearchDto.getTopic().isEmpty())
                queryString.append(" and course.topic.id = ").append(lectureSearchDto.getTopic());

            if (!lectureSearchDto.getSort().isEmpty())
                queryString.append(" order by ").append(lectureSearchDto.getSort());


            Query<Lecture> query = session.createQuery(queryString.toString(), Lecture.class);

            return query.list();
        }
    }

    @Override
    public List<Lecture> getAllCourseLectures(int courseId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Lecture> query = session.createQuery("from Lecture where course.id = :courseId", Lecture.class);
            query.setParameter("courseId", courseId);

            return query.list();
        }
    }
}
