package com.telerikacademy.web.chefmeup.services.contracts;

import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.StudentLecture;
import com.telerikacademy.web.chefmeup.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface StudentLectureService {
    StudentLecture getByStudentAndLecture(int userId, int lectureId);

    void submitAssignmentToLecture(StudentLecture studentLecture, MultipartFile file, User loggedUser);

    void assignLecturesToUser(int courseId, User loggedUser);

    void submitAssignment(MultipartFile file, Lecture lecture, User lu);

    List<StudentLecture> getTeachersLectureAssignments(User loggedUser);

    void gradeAssignment(User lu, int assignmentId, int grade);

    List<StudentLecture> getAllStudentsLectures(User loggedUser, int courseId);
}
