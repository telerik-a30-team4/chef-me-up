package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.exeptions.DuplicateEntityException;
import com.telerikacademy.web.chefmeup.exeptions.EntityNotFoundException;
import com.telerikacademy.web.chefmeup.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.chefmeup.models.Role;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.TeacherRequestDto;
import com.telerikacademy.web.chefmeup.models.params.UserSearchParams;
import com.telerikacademy.web.chefmeup.repositories.contracts.RoleRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.UserRepository;
import com.telerikacademy.web.chefmeup.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static final String UNAUTHORIZED_OPERATION_EXCEPTION_MSG = "You are not allowed to view or modify this profile.";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<User> getAll(User loggedUser) {
        if (loggedUser.isAdmin()) {
            return userRepository.getAll();
        } else {
            return userRepository.getAll().stream()
                    .filter(u -> !u.isAdmin())
                    .collect(Collectors.toList());
        }
    }

    @Override
    public User getById(int userId, User loggedUser) {
        if (loggedUser.getId() != userId && !loggedUser.isAdmin()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_OPERATION_EXCEPTION_MSG);
        }
        return userRepository.getById(userId);
    }

    @Override
    public  User getByEmail(String email) {
        return userRepository.getByEmail(email, 0);
    }

    @Override
    public User getByPhone(String phone) {
        return userRepository.getByPhone(phone, 0);
    }

    @Override
    public void create(User user) {
        if (checkIfUserEmailOrPhoneExists(user)) {
            throw new DuplicateEntityException("User", "this e-mail", "or phone");
        }

        userRepository.create(user);
    }

    @Override
    public void update(User userToUpdate, User loggedUser) {
        if (loggedUser.getId() != userToUpdate.getId() && !loggedUser.isAdmin()) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_OPERATION_EXCEPTION_MSG);
        }
        if (checkIfUserEmailOrPhoneExists(userToUpdate)) {
            throw new DuplicateEntityException("User", "this e-mail", "or phone");
        }

        userRepository.update(userToUpdate);
    }

    @Override
    public void delete(int userId, User loggedUser) {
        userRepository.delete(userId);
    }

    @Override
    public List<User> search(UserSearchParams userSearchParams, User loggedUser) {
        if (loggedUser.isAdmin()) {
            return userRepository.search(userSearchParams);
        } else {
            return userRepository.search(userSearchParams).stream().filter(u -> !u.isAdmin()).collect(Collectors.toList());
        }
    }

    @Override
    public void uploadUserProfilePicture(MultipartFile file, User user) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = user.getEmail() + "-" + "ProfilePic" + fileName.substring(fileName.lastIndexOf("."));
        Path resourceDirectory = Paths.get( "media","userpictures", saveAsName);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path profilePicPath = Paths.get(absolutePath);

        userRepository.uploadUserProfilePicture(file, user, profilePicPath, saveAsName);
    }

    @Override
    public List<User> getAllStudentsWithTeacherRequest() {
        return userRepository.getAllStudentsWithTeacherRequest().stream()
                .filter(u -> !u.isTeacher())
                .collect(Collectors.toList());
    }

    @Override
    public void teacherRequestApproval(TeacherRequestDto teacherRequestDto, int userId) {
        User userToUpdate = userRepository.getById(userId);

        if (teacherRequestDto.isRequestApproved()) {
            Set<Role> roleSet = userToUpdate.getRoles();
            Role role = roleRepository.getById(2);
            roleSet.add(role);
            userToUpdate.setRoles(roleSet);
            userToUpdate.setTeacherRequest(false);
        } else {
            userToUpdate.setTeacherRequest(false);
        }

        userRepository.update(userToUpdate);
    }

    private boolean checkIfUserEmailOrPhoneExists(User userToCheck){
        boolean isThereSuchEmail = true;
        try {
            userRepository.getByEmail(userToCheck.getEmail(), userToCheck.getId());
        } catch (EntityNotFoundException e) {
            isThereSuchEmail = false;
        }

        boolean isThereSuchPhone = true;
        try {
            userRepository.getByPhone(userToCheck.getPhone(), userToCheck.getId());
        } catch (EntityNotFoundException e) {
            isThereSuchPhone = false;
        }

        return isThereSuchEmail || isThereSuchPhone;
    }

}
