package com.telerikacademy.web.chefmeup.models.mappers;

import com.telerikacademy.web.chefmeup.models.Country;
import com.telerikacademy.web.chefmeup.models.Role;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.RegisterUserDto;
import com.telerikacademy.web.chefmeup.models.dtos.UserPasswordDto;
import com.telerikacademy.web.chefmeup.models.params.UserSearchParams;
import com.telerikacademy.web.chefmeup.repositories.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class UserMapper {

    private final UserRepository userRepository;
    private final CountryRepository countryRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserMapper(UserRepository userRepository,
                      CountryRepository countryRepository,
                      RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.countryRepository = countryRepository;
        this.roleRepository = roleRepository;
    }

    public User userFromDto(RegisterUserDto registerUserDto) {
        User user = new User();
        userDtoToObject(registerUserDto, user);

        Set<Role> roleSet = new HashSet<>();
        Role role = roleRepository.getById(1);
        roleSet.add(role);

        if (user.getPicture() == null) {
            user.setPicture("default-ProfilePic.png");
        }

        user.setRoles(roleSet);

        return user;
    }

    public User userFromDto(RegisterUserDto registerUserDto, int id) {
        User user = userRepository.getById(id);
        userDtoToObject(registerUserDto, user);

        return user;
    }

    public RegisterUserDto registerUserDtoFromObject(User user) {
        RegisterUserDto registerUserDto = new RegisterUserDto();

        registerUserDto.setFirstName(user.getFirstName());
        registerUserDto.setLastName(user.getLastName());
        registerUserDto.setEmail(user.getEmail());
        registerUserDto.setPhone(user.getPhone());
        registerUserDto.setPicture(user.getPicture());
        registerUserDto.setCountryId(user.getCountry().getId());
        registerUserDto.setTeacherRequest(user.isTeacherRequest());

        return registerUserDto;
    }

    public User userChangePassword(User userToChange, UserPasswordDto userPasswordDto) {
        userToChange.setPassword(userPasswordDto.getPassword());
        return userToChange;
    }

    public UserSearchParams searchParamsToObject(Optional<String> nameOrEmail,
                                                 Optional<String> orderBy){
        UserSearchParams userSearchParams = new UserSearchParams();

        nameOrEmail.ifPresent(userSearchParams::setNameOrEmail);
        orderBy.ifPresent(userSearchParams::setOrderBy);

        return userSearchParams;
    }

    private void userDtoToObject(RegisterUserDto registerUserDto, User user) {
        Country country = countryRepository.getById(registerUserDto.getCountryId());

        user.setFirstName(registerUserDto.getFirstName());
        user.setLastName(registerUserDto.getLastName());
        user.setEmail(registerUserDto.getEmail());
        user.setPhone(registerUserDto.getPhone());
        user.setTeacherRequest(registerUserDto.isTeacherRequest());
        user.setCountry(country);
        if (registerUserDto.getPicture() != null) {
            user.setPicture(registerUserDto.getPicture());
        }
    }

}
