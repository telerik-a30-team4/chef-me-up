package com.telerikacademy.web.chefmeup.models.params;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserSearchParams {

    private String nameOrEmail;
    private String orderBy;

}
