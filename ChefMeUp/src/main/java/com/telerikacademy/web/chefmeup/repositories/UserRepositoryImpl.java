package com.telerikacademy.web.chefmeup.repositories;

import com.telerikacademy.web.chefmeup.exeptions.EntityNotFoundException;
import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.params.UserSearchParams;
import com.telerikacademy.web.chefmeup.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

@Repository
public class UserRepositoryImpl extends BaseModifyRepositoryImpl<User> implements UserRepository {

    private static final String STORE_FILE_ERROR_MSG = "Could not store file. Please try again!";

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, User.class);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User getByEmail(String email, int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email and id != :userId", User.class);
            query.setParameter("email", email);
            query.setParameter("userId", userId);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return users.get(0);
        }
    }

    @Override
    public User getByPhone(String phone, int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where phone = :phone and id != :userId", User.class);
            query.setParameter("phone", phone);
            query.setParameter("userId", userId);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "phone", phone);
            }

            return users.get(0);
        }
    }

    @Override
    public void uploadUserProfilePicture(MultipartFile file, User user, Path picPath, String fileName) {
        try {
            Files.copy(file.getInputStream(), picPath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new NotAllowedOperationException(STORE_FILE_ERROR_MSG);
        }

        user.setPicture(fileName);
        update(user);
    }

    @Override
    public List<User> getAllStudentsWithTeacherRequest() {
        try (Session session = sessionFactory.openSession()) {

            Query<User> query = session.createQuery("From User where teacherRequest = true", User.class);

            return query.list();
        }
    }

    @Override
    public List<User> search(UserSearchParams userSearchParams) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder baseQuery = new StringBuilder().append("from User where 1=1");

            if (!userSearchParams.getNameOrEmail().isEmpty())
                baseQuery.append(" and concat(firstName, ' ', lastName, ' ', email) like ").append("'%")
                        .append(userSearchParams.getNameOrEmail()).append("%'");

            if (!userSearchParams.getOrderBy().isEmpty())
                baseQuery.append(" order by ")
                        .append(userSearchParams.getOrderBy());

            Query<User> query = session.createQuery(baseQuery.toString(), User.class);

            return query.list();
        }
    }
}
