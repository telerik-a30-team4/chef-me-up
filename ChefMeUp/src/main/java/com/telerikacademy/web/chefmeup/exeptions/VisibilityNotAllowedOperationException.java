package com.telerikacademy.web.chefmeup.exeptions;

public class VisibilityNotAllowedOperationException extends RuntimeException {
    public VisibilityNotAllowedOperationException(String message) {
        super(message);
    }
}
