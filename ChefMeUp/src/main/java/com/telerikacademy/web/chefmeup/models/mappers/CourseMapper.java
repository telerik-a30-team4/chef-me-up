package com.telerikacademy.web.chefmeup.models.mappers;

import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.dtos.CourseDto;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@Component
public class CourseMapper {

    public static final String DATE_ERROR_MSG = "Date is not in the required format.(yyyy-MM-dd)";

    private final CourseRepository courseRepository;
    private final TopicRepository topicRepository;

    @Autowired
    public CourseMapper(CourseRepository courseRepository,
                        TopicRepository topicRepository) {
        this.courseRepository = courseRepository;
        this.topicRepository = topicRepository;
    }

    public Course courseFromDto(CourseDto courseDto) {
        if (courseDto.getVideo().contains("?v=")) {
            embedLink(courseDto);
        }
        Course course = new Course();
        courseDtoToObject(courseDto, course);

        return course;
    }

    public Course courseFromDto(CourseDto courseDto, int id) {
        if (courseDto.getVideo().contains("?v=")) {
            embedLink(courseDto);
        }
        Course course = courseRepository.getById(id);
        courseDtoToObject(courseDto, course);

        return course;
    }

    public CourseDto courseDtoFromObject(Course course) {
        CourseDto courseDto = new CourseDto();
        courseDto.setDescription(course.getDescription());
        courseDto.setTitle(course.getTitle());
        courseDto.setVisible(course.isVisible());
        courseDto.setPicture(course.getPicture());
        courseDto.setVideo(course.getVideo());
        courseDto.setTopicId(course.getTopic().getId());
        if (course.getStartingDate() != null && course.getExpireDate() != null) {
            courseDto.setExpireDate(course.getExpireDate().toString());
            courseDto.setStartingDate(course.getStartingDate().toString());
        }
        return courseDto;
    }

    private void courseDtoToObject(CourseDto courseDto, Course course) {
        course.setTitle(courseDto.getTitle());
        course.setTopic(topicRepository.getById(courseDto.getTopicId()));
        course.setDescription(courseDto.getDescription());
        course.setVisible(courseDto.isVisible());
        course.setVideo(courseDto.getVideo());
        if (courseDto.getPicture() != null) {
            course.setPicture(courseDto.getPicture());
        }

        try {
            if (!courseDto.getStartingDate().isEmpty()) {
                course.setStartingDate(LocalDate.parse(courseDto.getStartingDate()));
            }
            if (!courseDto.getExpireDate().isEmpty()) {
                course.setExpireDate(LocalDate.parse(courseDto.getExpireDate()));
            }
        } catch (DateTimeParseException e) {
            throw new NotAllowedOperationException(DATE_ERROR_MSG);
        }
    }

    private void embedLink(CourseDto courseDto) {
        String url = courseDto.getVideo();
        String id = url.substring(url.lastIndexOf("?v=") + 3);
        String embedlink = "http://www.youtube.com/embed/" + id;
        courseDto.setVideo(embedlink);
    }

}
