package com.telerikacademy.web.chefmeup.models.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
@NoArgsConstructor
public class RegisterUserDto {

    @NotBlank(message = "First name can't be empty")
    @Size(min = 3, max = 15, message = "First name should be between 3 and 15 symbols")
    private String firstName;

    @NotBlank(message = "Last name can't be empty")
    @Size(min = 3, max = 15, message = "Last name should be between 3 and 15 symbols")
    private String lastName;

    @NotBlank(message = "Email can't be empty")
    @Email(message = "Email should be valid")
    private String email;

    @NotBlank(message = "Phone number can't be empty")
    @Pattern(regexp = "08[789]\\d{7}", message = "Phone number must be 08xxxxxxxx!")
    @Size(min = 10, max = 15, message = "Phone number should be between 10 and 15 digits")
    private String phone;

    private String picture;

    @NotNull(message = "Country can't be empty")
    @Positive
    private int countryId;

    private boolean teacherRequest;

}
