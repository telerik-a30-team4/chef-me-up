package com.telerikacademy.web.chefmeup.services.contracts;

import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.LectureSearchDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface LectureService {
    List<Lecture> getAllCourseLectures(int courseId);

    Lecture getById(int lectureId, User user);

    void update(Lecture lecture, User loggedUser);

    void delete(int lectureId, User loggedUser);

    void create(Lecture lecture, User loggedUser);

    void uploadAssignment(MultipartFile file, Lecture lecture, User loggedUser);

    void uploadLecturePicture(MultipartFile file, Lecture newLecture);

    List<Lecture> getAllTeacherLectures(User lu);

    List<Lecture> search(LectureSearchDto lectureSearchDto);
}
