package com.telerikacademy.web.chefmeup.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "courses")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "course_id")
    private int id;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User creator;

    @Column(name = "starting_date")
    private LocalDate startingDate;

    @Column(name = "expire_date")
    private LocalDate expireDate;

    @Column(name = "visible")
    private boolean visible;

    @Column(name = "picture")
    private String picture;

    @Column(name = "video")
    private String video;

}
