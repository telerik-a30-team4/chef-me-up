package com.telerikacademy.web.chefmeup.controllers.mvc;

import com.telerikacademy.web.chefmeup.config.security.AppUserDetails;
import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.exeptions.VisibilityNotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.*;
import com.telerikacademy.web.chefmeup.models.dtos.CourseDto;
import com.telerikacademy.web.chefmeup.models.dtos.CourseSearchDto;
import com.telerikacademy.web.chefmeup.models.dtos.RateStudentCourseDto;
import com.telerikacademy.web.chefmeup.models.mappers.CourseMapper;
import com.telerikacademy.web.chefmeup.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/courses")
public class CourseMvcController {

    private final CourseService courseService;
    private final LectureService lectureService;
    private final CourseMapper courseMapper;
    private final TopicService topicService;
    private final StudentCourseService studentCourseService;
    private final StudentLectureService studentLectureService;

    @Autowired
    public CourseMvcController(CourseService courseService,
                               LectureService lectureService,
                               CourseMapper courseMapper,
                               TopicService topicService,
                               StudentCourseService studentCourseService,
                               StudentLectureService studentLectureService) {
        this.courseService = courseService;
        this.lectureService = lectureService;
        this.courseMapper = courseMapper;
        this.topicService = topicService;
        this.studentCourseService = studentCourseService;
        this.studentLectureService = studentLectureService;
    }

    @ModelAttribute("topics")
    public List<Topic> populateTopics() {
        return topicService.getAll();
    }

    @GetMapping
    public String showAllCourses(Model model) {
        model.addAttribute("courseRatings", courseService.getAllCourseRatings());
        model.addAttribute("courseSearchDto", new CourseSearchDto());
        model.addAttribute("courses", courseService.getAllOnlineCourses());
        return "courses";
    }

    @GetMapping("/{courseId}")
    public String showSingleCourse(@PathVariable int courseId,
                                   Model model,
                                   @AuthenticationPrincipal AppUserDetails user) {
        Course course = courseService.getById(courseId);
        if (user != null) {
            User loggedUser = user.getUser();
            List<Lecture> courseLectures = lectureService.getAllCourseLectures(courseId);
            model.addAttribute("isRated", studentCourseService.isCourseRated(courseId, loggedUser));
            model.addAttribute("isUserEnrolled", studentCourseService.isUserEnrolled(courseId, loggedUser));
            model.addAttribute("courseLectures", courseLectures);
            model.addAttribute("isCourseStarted", courseService.isCourseStarted(courseId));
        }
        model.addAttribute("rating", new RateStudentCourseDto());
        model.addAttribute("course", course);

        return "course";
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/new")
    public String showNewCoursePage(Model model) {
        model.addAttribute("course", new CourseDto());
        return "course-new";
    }

    @Secured("ROLE_TEACHER")
    @PostMapping("/new")
    public String createCourse(@Valid @ModelAttribute("course") CourseDto courseDto,
                               BindingResult errors,
                               @AuthenticationPrincipal AppUserDetails user,
                               @RequestParam("file") MultipartFile file) {
        if (errors.hasErrors()) {
            return "course-new";
        }

        try {
            User loggedUser = user.getUser();
            Course newCourse = courseMapper.courseFromDto(courseDto);

            courseService.create(newCourse, loggedUser);
            courseService.uploadCoursePicture(file, newCourse, loggedUser);
        } catch (NotAllowedOperationException e) {
            errors.rejectValue("expireDate", "date_error", e.getMessage());
            return "course-new";
        } catch (VisibilityNotAllowedOperationException e) {
            errors.rejectValue("visible", "visible_error", e.getMessage());
            return "course-new";
        }
        return "redirect:/courses";
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/{courseId}/update")
    public String showUpdateCoursePage(@PathVariable int courseId,
                                       Model model) {
        Course course = courseService.getById(courseId);
        CourseDto courseDto = courseMapper.courseDtoFromObject(course);
        model.addAttribute("course", courseDto);
        return "course-update";
    }

    @Secured("ROLE_TEACHER")
    @PostMapping("/{courseId}/update")
    public String updateCourse(@PathVariable int courseId,
                               @Valid @ModelAttribute("course") CourseDto courseDto,
                               BindingResult errors,
                               @RequestParam("file") MultipartFile file,
                               @AuthenticationPrincipal AppUserDetails user) {
        if (errors.hasErrors()) {
            return "course-update";
        }

        try {
            User loggedUser = user.getUser();
            Course newCourse = courseMapper.courseFromDto(courseDto, courseId);

            courseService.update(newCourse, loggedUser);
            if (!file.isEmpty()) {
                courseService.uploadCoursePicture(file, newCourse, loggedUser);
            }

        } catch (NotAllowedOperationException e) {
            errors.rejectValue("expireDate", "date_error", e.getMessage());
            return "course-update";
        } catch (VisibilityNotAllowedOperationException e) {
            errors.rejectValue("visible", "visible_error", e.getMessage());
            return "course-update";
        }
        return "redirect:/courses/" + courseId;
    }

    @Secured("ROLE_STUDENT")
    @PostMapping("/{courseId}/enroll")
    public String enrollCourse(@PathVariable int courseId,
                               @AuthenticationPrincipal AppUserDetails user) {
        User loggedUser = user.getUser();
        studentCourseService.enroll(courseId, loggedUser);
        studentLectureService.assignLecturesToUser(courseId, loggedUser);

        return "redirect:/courses/" + courseId;
    }

    @Secured("ROLE_STUDENT")
    @PostMapping("/{courseId}/rate")
    public String rateCourse(@PathVariable int courseId,
                             @Valid @ModelAttribute("rating") RateStudentCourseDto rateStudentCourseDto,
                             @AuthenticationPrincipal AppUserDetails user) {
        User loggedUser = user.getUser();
        studentCourseService.rateCourse(courseId, loggedUser, rateStudentCourseDto.getCourseRating());
        return "redirect:/courses/" + courseId;
    }

    @Secured("ROLE_TEACHER")
    @GetMapping("/dashboard")
    public String getTeachersCourses(@AuthenticationPrincipal AppUserDetails user,
                                     Model model) {
        User loggedUser = user.getUser();
        List<Course> teacherCourses = courseService.getAllTeacherCourses(loggedUser);
        model.addAttribute("courses", teacherCourses);
        model.addAttribute("courseSearchDto", new CourseSearchDto());

        return "course-dashboard";
    }

    @PostMapping("/dashboard/search")
    public String searchCourseDashboard(@ModelAttribute("courseSearchDto") CourseSearchDto courseSearchDto,
                                  Model model) {
        List<Course> searchCourses = courseService.search(courseSearchDto);

        model.addAttribute("courses", searchCourses);

        return "course-dashboard";
    }

    @Secured("ROLE_STUDENT")
    @GetMapping("/enrolled")
    public String getAllUsersEnrolledCourses(@AuthenticationPrincipal AppUserDetails user,
                                             Model model) {
        User loggedUser = user.getUser();
        List<Course> enrolledCourses = courseService.getAllUsersEnrolledCourses(loggedUser);
        model.addAttribute("grades", courseService.getUsersCourseGrade(loggedUser));
        model.addAttribute("courses", enrolledCourses);

        return "enrolled-courses";
    }

    @PostMapping("/search")
    public String searchForCourse(@ModelAttribute("courseSearchDto") CourseSearchDto courseSearchDto,
                                  Model model) {
        courseSearchDto.setOnline(true);
        List<Course> searchCourses = courseService.search(courseSearchDto);

        model.addAttribute("courses", searchCourses);
        model.addAttribute("courseRatings", courseService.getAllCourseRatings());

        return "courses";
    }

    @GetMapping("/{courseId}/lectures")
    public String showCourseLecturesDashboard(@PathVariable int courseId,
                                              Model model,
                                              @AuthenticationPrincipal AppUserDetails appUserDetails) {
        User loggedUser = appUserDetails.getUser();
        List<StudentLecture> studentLectures = studentLectureService.getAllStudentsLectures(loggedUser, courseId);
        model.addAttribute("lectures", studentLectures);

        return "students-lectures";
    }

}
