package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.LectureSearchDto;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.LectureRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentCourseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;

import static com.telerikacademy.web.chefmeup.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class LectureServiceImplTests {

    @Mock
    LectureRepository mockLectureRepository;

    @Mock
    CourseRepository mockCourseRepository;

    @Mock
    StudentCourseRepository studentCourseRepository;

    @InjectMocks
    LectureServiceImpl mockLectureServiceImpl;

    @Test
    public void getAllCourseLectures_should_callRepository() {
        Course mockCourse = createMockCourse();

        Mockito.when(mockLectureRepository.getAllCourseLectures(mockCourse.getId()))
                .thenReturn(new ArrayList<>());

        mockLectureServiceImpl.getAllCourseLectures(mockCourse.getId());

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).getAllCourseLectures(mockCourse.getId());
    }

    @Test
    public void getById_should_callRepository() {
        User mockStudent = createMockStudent();
        Course mockCourse = createMockCourse();
        Lecture mockLecture = createMockLecture();

        Mockito.when(mockLectureRepository.getCourseByLecture(mockLecture.getId()))
                .thenReturn(mockCourse);
        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        mockLectureServiceImpl.getById(mockLecture.getId(), mockStudent);

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).getById(mockLecture.getId());
    }

    @Test
    public void getById_should_throw_when_studentIsNotAssignedToTheLecture() {
        User mockStudent = createMockStudent();
        Course mockCourse = createMockCourse();
        Lecture mockLecture = createMockLecture();
        mockCourse.getCreator().setId(2);


        Mockito.when(mockLectureRepository.getCourseByLecture(mockLecture.getId()))
                .thenReturn(mockCourse);
        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockLectureServiceImpl.getById(mockLecture.getId(), mockStudent));
    }

    @Test
    public void create_should_callRepository() {
        User mockStudent = createMockStudent();
        Lecture mockLecture = createMockLecture();
        Course mockCourse = createMockCourse();

        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        mockLectureServiceImpl.create(mockLecture, mockStudent);

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).create(mockLecture);
    }

    @Test
    public void create_should_throw_ifUserIsNotCreatorOfCourse() {
        User mockStudent = createMockStudent();
        Lecture mockLecture = createMockLecture();
        Course mockCourse = createMockCourse();
        mockCourse.getCreator().setId(2);

        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);
        Mockito.when(mockLectureRepository.getCourseByLecture(mockLecture.getId()))
                .thenReturn(mockCourse);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockLectureServiceImpl.getById(mockLecture.getId(), mockStudent));
    }

    @Test
    public void getAllTeacherLectures_should_callRepository_when_userIsTeacher() {
        User mockTeacher = createMockTeacher();

        mockLectureServiceImpl.getAllTeacherLectures(mockTeacher);

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).getAllTeacherLectures(mockTeacher);
    }

    @Test
    public void getAllTeacherLectures_should_callRepositoryWhenUserIsAdmin() {
        User mockAdmin = createMockAdmin();

        mockLectureServiceImpl.getAllTeacherLectures(mockAdmin);

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void search_should_callRepository() {
        LectureSearchDto lectureSearchDto = new LectureSearchDto();

        mockLectureServiceImpl.search(lectureSearchDto);

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).search(lectureSearchDto);
    }

    @Test
    public void update_should_callRepository() {
        User mockTeacher = createMockTeacher();
        Lecture mockLecture = createMockLecture();
        Course mockCourse = createMockCourse();

        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        mockLectureServiceImpl.update(mockLecture, mockTeacher);

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).update(mockLecture);
    }

    @Test
    public void delete_should_callRepository() {
        User mockTeacher = createMockTeacher();
        Lecture mockLecture = createMockLecture();
        Course mockCourse = createMockCourse();

        Mockito.when(mockLectureRepository.getCourseByLecture(mockLecture.getId()))
                .thenReturn(mockCourse);
        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        mockLectureServiceImpl.delete(mockLecture.getId(), mockTeacher);

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).delete(mockLecture.getId());

    }

    @Test
    public void delete_throw_when_userIsNotCreatorOrAdmin() {
        User mockTeacher = createMockTeacher();
        mockTeacher.setId(2);
        Lecture mockLecture = createMockLecture();
        Course mockCourse = createMockCourse();

        Mockito.when(mockLectureRepository.getCourseByLecture(mockLecture.getId()))
                .thenReturn(mockCourse);
        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockLectureServiceImpl.delete(mockLecture.getId(), mockTeacher));

    }

    @Test
    public void uploadPicture_should_callRepository() {
        Lecture mockLecture = createMockLecture();
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = mockLecture.getTitle() + "-" + "BACKGROUND" +
                fileName.substring(fileName.lastIndexOf("."));
        String name = saveAsName.replaceAll("\\s+", "");
        Path resourceDirectory = Paths.get( "media","lecturebackground", name);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path assignments = Paths.get(absolutePath);

        mockLectureServiceImpl.uploadLecturePicture(file, mockLecture);

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).uploadLecturePicture(file, mockLecture, name, assignments);

    }

    @Test
    public void uploadAssignment_should_callRepository() {
        Lecture mockLecture = createMockLecture();
        User mockStudent = createMockStudent();
        Course mockCourse = createMockCourse();
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = mockLecture.getCourse().getTitle() + "-" +
                mockLecture.getTitle() + "-" + "assignment" +
                fileName.substring(fileName.lastIndexOf("."));
        String name = saveAsName.replaceAll(" ", "");
        Path resourceDirectory = Paths.get("media", "lectureassignments", name);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path assignments = Paths.get(absolutePath);

        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        mockLectureServiceImpl.uploadAssignment(file, mockLecture, mockStudent);

        Mockito.verify(mockLectureRepository,
                Mockito.times(1)).uploadAssignment(file, mockLecture, name, assignments);

    }

}
