package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.Helpers;
import com.telerikacademy.web.chefmeup.exeptions.DuplicateEntityException;
import com.telerikacademy.web.chefmeup.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.StudentLecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentLectureRepository;
import com.telerikacademy.web.chefmeup.services.contracts.StudentLectureService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@ExtendWith(MockitoExtension.class)
public class StudentLectureServiceImplTests {

    @Mock
    StudentLectureRepository mockStudentLectureRepository;

    @InjectMocks
    StudentLectureServiceImpl mockStudentLectureService;

    @Test
    void getByStudentAndLecture_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();
        Lecture mockLecture = Helpers.createMockLecture();

        mockStudentLectureService.getByStudentAndLecture(mockStudent.getId(), mockLecture.getId());

        Mockito.verify(mockStudentLectureRepository,
                Mockito.times(1)).getByStudentAndLecture(mockStudent.getId(), mockLecture.getId());
    }

    @Test
    void submitAssignmentToLecture_should_callRepository_whenStudentIsLoggedUser() {
        User mockStudent = Helpers.createMockStudent();
        StudentLecture mockStudentLecture = new StudentLecture();
        mockStudentLecture.setStudent(mockStudent);
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        mockStudentLectureService.submitAssignmentToLecture(mockStudentLecture, file, mockStudent);

        Mockito.verify(mockStudentLectureRepository,
                Mockito.times(1)).submitAssignmentToLecture(mockStudentLecture, file);
    }

    @Test
    void submitAssignmentToLecture_should_throw_whenStudentIsNotLoggedUser() {
        User mockStudent = Helpers.createMockStudent();
        User mockStudent2 = Helpers.createMockStudent();
        mockStudent2.setId(2);

        StudentLecture mockStudentLecture = new StudentLecture();
        mockStudentLecture.setStudent(mockStudent);

        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockStudentLectureService.submitAssignmentToLecture(mockStudentLecture, file, mockStudent2));
    }

    @Test
    void submitAssignment_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();
        Lecture mockLecture = Helpers.createMockLecture();
        StudentLecture mockStudentLecture = new StudentLecture();
        mockStudentLecture.setStudent(mockStudent);
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = mockLecture.getCourse().getTitle() + "-" +
                mockLecture.getTitle() + "-" +
                mockStudent.getEmail() + "." +
                fileName.substring(fileName.lastIndexOf(".") + 1);
        String name = saveAsName.replaceAll(" ", "");
        Path resourceDirectory = Paths.get( "media","studentsassignments", name);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path assignments = Paths.get(absolutePath);

        mockStudentLectureService.submitAssignment(file, mockLecture, mockStudent);

        Mockito.verify(mockStudentLectureRepository,
                Mockito.times(1)).submitAssignment(file, mockLecture, mockStudent, name, assignments);
    }

    @Test
    void getTeachersLectureAssignments_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();

        mockStudentLectureService.getTeachersLectureAssignments(mockStudent);

        Mockito.verify(mockStudentLectureRepository,
                Mockito.times(1)).getTeachersLectureAssignments(mockStudent);
    }

    @Test
    void gradeAssignment_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();
        StudentLecture mockAssignment = new StudentLecture();

        Mockito.when(mockStudentLectureRepository.getById(mockAssignment.getId()))
                .thenReturn(mockAssignment);

        mockStudentLectureService.gradeAssignment(mockStudent, mockAssignment.getId(), 100);

        Mockito.verify(mockStudentLectureRepository,
                Mockito.times(1)).update(mockAssignment);
    }

    @Test
    void getAllStudentsLectures_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();
        Course mockCourse = Helpers.createMockCourse();

        mockStudentLectureService.getAllStudentsLectures(mockStudent, mockCourse.getId());

        Mockito.verify(mockStudentLectureRepository,
                Mockito.times(1)).getAllStudentsLectures(mockStudent, mockCourse.getId());
    }

}
