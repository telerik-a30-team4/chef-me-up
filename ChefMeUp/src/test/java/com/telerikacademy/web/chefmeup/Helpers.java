package com.telerikacademy.web.chefmeup;

import com.telerikacademy.web.chefmeup.models.*;

import java.util.HashSet;
import java.util.Set;

public class Helpers {

    public static User createMockUser(Set<Role> roles) {
        User mockUser = new User();
        mockUser.setId(1);
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setEmail("mock@user.com");
        mockUser.setRoles(roles);

        return mockUser;
    }

    public static User createMockStudent() {
        return createMockUser(createMockStudentRole());
    }

    public static User createMockTeacher() {
        return createMockUser(createMockTeacherRole());
    }

    public static User createMockAdmin() {
        return createMockUser(createMockAdminRole());
    }

    private static Set<Role> createMockStudentRole() {
        Set<Role> roles = new HashSet<>();
        Role roleStudent = new Role();
        roleStudent.setId(1);
        roleStudent.setName("ROLE_STUDENT");
        roles.add(roleStudent);
        return roles;
    }

    private static Set<Role> createMockTeacherRole() {
        Role roleTeacher = new Role();
        roleTeacher.setId(2);
        roleTeacher.setName("ROLE_TEACHER");
        Set<Role> roles = createMockStudentRole();
        roles.add(roleTeacher);
        return roles;
    }

    private static Set<Role> createMockAdminRole() {
        Role roleAdmin = new Role();
        roleAdmin.setId(3);
        roleAdmin.setName("ROLE_ADMIN");
        Set<Role> roles = createMockTeacherRole();
        roles.add(roleAdmin);
        return roles;
    }

    public static Course createMockCourse() {
        Course mockCourse = new Course();
        mockCourse.setId(1);
        mockCourse.setTitle("MockCourse");
        mockCourse.setCreator(createMockTeacher());
        mockCourse.setTopic(createMockTopic());
        mockCourse.setDescription("VeryVeryLongMockDescriptionYesVeryVeryVeryLong");
        mockCourse.setVisible(false);
        return mockCourse;
    }

    private static Topic createMockTopic() {
        Topic mockTopic = new Topic();
        mockTopic.setId(1);
        mockTopic.setName("MockTopic");
        return mockTopic;
    }

    public static Lecture createMockLecture() {
        Lecture mockLecture = new Lecture();
        mockLecture.setId(1);
        mockLecture.setCourse(createMockCourse());
        mockLecture.setTitle("MockLEcture");
        return mockLecture;
    }

}
