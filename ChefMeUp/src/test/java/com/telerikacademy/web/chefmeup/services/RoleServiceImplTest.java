package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.repositories.contracts.RoleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class RoleServiceImplTest {

    @Mock
    RoleRepository mockRoleRepository;

    @InjectMocks
    RoleServiceImpl mockRoleSerivice;

    @Test
    public void getById_should_callRepository() {
        mockRoleSerivice.getById(1);

        Mockito.verify(mockRoleRepository,
                Mockito.times(1)).getById(1);
    }
}
