package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.Helpers;
import com.telerikacademy.web.chefmeup.exeptions.DuplicateEntityException;
import com.telerikacademy.web.chefmeup.exeptions.EntityNotFoundException;
import com.telerikacademy.web.chefmeup.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.chefmeup.models.Role;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.TeacherRequestDto;
import com.telerikacademy.web.chefmeup.models.params.UserSearchParams;
import com.telerikacademy.web.chefmeup.repositories.contracts.RoleRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockUserRepository;

    @Mock
    RoleRepository mockRoleRepository;

    @InjectMocks
    UserServiceImpl mockUserService;

    @Test
    void getAll_should_callRepository_whenLoggedUserIsAdmin() {
        User mockAdmin = Helpers.createMockAdmin();

        Mockito.when(mockUserRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockUserService.getAll(mockAdmin);

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    void getAll_should_callRepository_whenLoggedUserIsTeacher() {
        User mockTeacher = Helpers.createMockTeacher();

        Mockito.when(mockUserRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockUserService.getAll(mockTeacher);

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_callRepository_whenLoggedUserIsAdmin() {
        User mockAdmin = Helpers.createMockAdmin();
        User mockStudent = Helpers.createMockStudent();

        Mockito.when(mockUserRepository.getById(mockStudent.getId()))
                .thenReturn(mockStudent);

        User resultUser = mockUserService.getById(mockStudent.getId(), mockAdmin);

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockStudent.getId(), resultUser.getId()),
                () -> Assertions.assertEquals(mockStudent.getFirstName(), resultUser.getFirstName()),
                () -> Assertions.assertEquals(mockStudent.getLastName(), resultUser.getLastName()),
                () -> Assertions.assertEquals(mockStudent.getEmail(), resultUser.getEmail()),
                () -> Assertions.assertEquals(mockStudent.getPhone(), resultUser.getPhone())
        );
    }

    @Test
    public void getById_should_callRepository_whenLoggedUserIsTheSameUser() {
        User mockStudent = Helpers.createMockStudent();

        Mockito.when(mockUserRepository.getById(mockStudent.getId()))
                .thenReturn(mockStudent);

        User resultUser = mockUserService.getById(mockStudent.getId(), mockStudent);

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockStudent.getId(), resultUser.getId()),
                () -> Assertions.assertEquals(mockStudent.getFirstName(), resultUser.getFirstName()),
                () -> Assertions.assertEquals(mockStudent.getLastName(), resultUser.getLastName()),
                () -> Assertions.assertEquals(mockStudent.getEmail(), resultUser.getEmail()),
                () -> Assertions.assertEquals(mockStudent.getPhone(), resultUser.getPhone())
        );
    }

    @Test
    public void getById_should_throw_whenLoggedUserIsNotAdminNorTheSameUser() {
        User mockStudent = Helpers.createMockStudent();
        User mockStudent2 = Helpers.createMockStudent();
        mockStudent2.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserService.getById(mockStudent.getId(), mockStudent2));
    }

    @Test
    void getByEmail_should_callRepository() {
        User mockAdmin = Helpers.createMockAdmin();

        Mockito.when(mockUserRepository.getByEmail(mockAdmin.getEmail(), 0))
                .thenReturn(mockAdmin);

        mockUserService.getByEmail(mockAdmin.getEmail());

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getByEmail(mockAdmin.getEmail(), 0);
    }

    @Test
    void getByPhone_should_callRepository() {
        User mockAdmin = Helpers.createMockAdmin();

        Mockito.when(mockUserRepository.getByPhone(mockAdmin.getPhone(), 0))
                .thenReturn(mockAdmin);

        mockUserService.getByPhone(mockAdmin.getPhone());

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getByPhone(mockAdmin.getPhone(), 0);
    }

    @Test
    public void create_should_callRepository_when_UserEmailOrPhoneDoesNotExists() {
        User mockStudent = Helpers.createMockStudent();

        Mockito.when(mockUserRepository.getByEmail(mockStudent.getEmail(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByPhone(mockStudent.getPhone(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);

        mockUserService.create(mockStudent);

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).create(mockStudent);
    }

    @Test
    public void create_should_throw_when_UserEmailExists() {
        User mockStudent = Helpers.createMockStudent();

        Mockito.when(mockUserRepository.getByEmail(mockStudent.getEmail(), mockStudent.getId()))
                .thenReturn(mockStudent);

        Mockito.when(mockUserRepository.getByPhone(mockStudent.getPhone(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.create(mockStudent));
    }

    @Test
    public void create_should_throw_when_UserPhoneExists() {
        User mockStudent = Helpers.createMockStudent();

        Mockito.when(mockUserRepository.getByEmail(mockStudent.getEmail(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockUserRepository.getByPhone(mockStudent.getPhone(), mockStudent.getId()))
                .thenReturn(mockStudent);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.create(mockStudent));
    }

    @Test
    public void update_should_callRepository_when_LoggedUserIsAdmin() {
        User mockAdmin = Helpers.createMockAdmin();
        User mockStudent = Helpers.createMockStudent();
        mockStudent.setId(2);

        Mockito.when(mockUserRepository.getByEmail(mockStudent.getEmail(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(mockUserRepository.getByPhone(mockStudent.getPhone(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);

        mockUserService.update(mockStudent, mockAdmin);

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(mockStudent);

    }

    @Test
    public void update_should_callRepository_when_LoggedUserIsNotAdminButTheSameUser() {
        User mockStudent = Helpers.createMockStudent();

        Mockito.when(mockUserRepository.getByEmail(mockStudent.getEmail(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(mockUserRepository.getByPhone(mockStudent.getPhone(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);

        mockUserService.update(mockStudent, mockStudent);

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(mockStudent);
    }

    @Test
    public void update_should_throw_when_LoggedUserIsNotAdminNorTheSameUser() {
        User mockStudent = Helpers.createMockStudent();
        User mockStudent2 = Helpers.createMockStudent();
        mockStudent2.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockUserService.update(mockStudent, mockStudent2));
    }

    @Test
    public void update_should_throw_when_userToUpdateEmailExists() {
        User mockStudent = Helpers.createMockStudent();

        Mockito.when(mockUserRepository.getByEmail(mockStudent.getEmail(), mockStudent.getId()))
                .thenReturn(mockStudent);
        Mockito.when(mockUserRepository.getByPhone(mockStudent.getPhone(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.update(mockStudent, mockStudent));
    }

    @Test
    public void update_should_throw_when_userToUpdatePhoneExists() {
        User mockStudent = Helpers.createMockStudent();

        Mockito.when(mockUserRepository.getByEmail(mockStudent.getEmail(), mockStudent.getId()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(mockUserRepository.getByPhone(mockStudent.getPhone(), mockStudent.getId()))
                .thenReturn(mockStudent);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockUserService.update(mockStudent, mockStudent));
    }

    @Test
    void delete_should_callRepository() {
        User mockAdmin = Helpers.createMockAdmin();
        User mockStudent = Helpers.createMockStudent();

        mockUserService.delete(mockStudent.getId(), mockAdmin);

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).delete(mockStudent.getId());
    }

    @Test
    void search_should_callRepository_whenLoggedUserIsAdmin() {
        User mockAdmin = Helpers.createMockAdmin();
        UserSearchParams userSearchParams = new UserSearchParams();

        mockUserService.search(userSearchParams, mockAdmin);

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).search(userSearchParams);
    }

    @Test
    void search_should_callRepository_whenLoggedUserIsTeacher() {
        User mockTeacher = Helpers.createMockTeacher();
        UserSearchParams userSearchParams = new UserSearchParams();

        mockUserService.search(userSearchParams, mockTeacher);

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).search(userSearchParams);
    }

    @Test
    void getAllStudentsWithTeacherRequest_should_callRepository() {

        mockUserService.getAllStudentsWithTeacherRequest();

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).getAllStudentsWithTeacherRequest();
    }

    @Test
    void teacherRequestApproval_should_callRepository_ifRequestIsApproved() {
        User mockStudent = Helpers.createMockStudent();

        TeacherRequestDto teacherRequestDto = new TeacherRequestDto();
        teacherRequestDto.setRequestApproved(true);

        Role roleTeacher = new Role();
        roleTeacher.setId(2);
        roleTeacher.setName("ROLE_TEACHER");

        Mockito.when(mockUserRepository.getById(mockStudent.getId()))
                .thenReturn(mockStudent);
        Mockito.when(mockRoleRepository.getById(2))
                .thenReturn(roleTeacher);

        mockUserService.teacherRequestApproval(teacherRequestDto, mockStudent.getId());

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(mockStudent);
    }

    @Test
    void teacherRequestApproval_should_callRepository_ifRequestIsNotApproved() {
        User mockStudent = Helpers.createMockStudent();

        TeacherRequestDto teacherRequestDto = new TeacherRequestDto();
        teacherRequestDto.setRequestApproved(false);

        Mockito.when(mockUserRepository.getById(mockStudent.getId()))
                .thenReturn(mockStudent);

        mockUserService.teacherRequestApproval(teacherRequestDto, mockStudent.getId());

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).update(mockStudent);
    }

    @Test
    public void uploadPicture_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();

        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = mockStudent.getEmail() + "-" + "ProfilePic" + fileName.substring(fileName.lastIndexOf("."));
        Path resourceDirectory = Paths.get( "media","userpictures", saveAsName);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path profilePicPath = Paths.get(absolutePath);

        mockUserService.uploadUserProfilePicture(file, mockStudent);

        Mockito.verify(mockUserRepository,
                Mockito.times(1)).uploadUserProfilePicture(file, mockStudent, profilePicPath, saveAsName);
    }



}
