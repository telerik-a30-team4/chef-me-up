package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.repositories.contracts.CountryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository mockCountryRepository;

    @InjectMocks
    CountryServiceImpl mockCountryService;

    @Test
    public void getAll_should_callRepository() {
        mockCountryService.getAll();

        Mockito.verify(mockCountryRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_callRepository() {
        mockCountryService.getById(1);

        Mockito.verify(mockCountryRepository,
                Mockito.times(1)).getById(1);
    }
}
