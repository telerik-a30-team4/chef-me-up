package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.repositories.contracts.TopicRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TopicServiceImplTests {

    @Mock
    TopicRepository mockTopicRepository;

    @InjectMocks
    TopicServiceImpl mockTopicService;

    @Test
    public void getAll_should_callRepository() {
        mockTopicService.getAll();

        Mockito.verify(mockTopicRepository,
                Mockito.times(1)).getAll();
    }

}
