package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.Helpers;
import com.telerikacademy.web.chefmeup.exeptions.DuplicateEntityException;
import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.StudentCourse;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentCourseRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StudentCourseServiceImplTests {

    @Mock
    StudentCourseRepository mockStudentCourseRepository;

    @Mock
    CourseRepository mockCourseRepository;

    @InjectMocks
    StudentCourseServiceImpl mockStudentCourseService;

    @Test
    void getAll_should_callRepository() {

        mockStudentCourseService.getAll();

        Mockito.verify(mockStudentCourseRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    void getStudentCourse_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();
        Course mockCourse = Helpers.createMockCourse();

        mockStudentCourseService.getStudentCourse(mockStudent.getId(), mockCourse.getId());

        Mockito.verify(mockStudentCourseRepository,
                Mockito.times(1)).getStudentCourse(mockStudent.getId(), mockCourse.getId());
    }

/*    @Test
    void enroll_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();
        Course mockCourse = Helpers.createMockCourse();
        StudentCourse studentCourse = new StudentCourse();
        studentCourse.setStudent(mockStudent);
        studentCourse.setCourse(mockCourse);

        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        mockStudentCourseService.enroll(mockCourse.getId(), mockStudent);

        Mockito.verify(mockStudentCourseRepository,
                Mockito.times(1)).enroll(studentCourse);
    }*/

    @Test
    void rateCourse_should_callRepository_whenAverageGradeIsAbove50() {
        User mockStudent = Helpers.createMockStudent();
        Course mockCourse = Helpers.createMockCourse();
        StudentCourse mockStudentCourse = new StudentCourse();

        Mockito.when(mockStudentCourseRepository.getStudentCourse(mockCourse.getId(), mockCourse.getId()))
                .thenReturn(mockStudentCourse);
        Mockito.when(mockStudentCourseRepository.getStudentCourseAverageGrade(mockCourse.getId(), mockStudent.getId()))
                .thenReturn(51d);

        mockStudentCourseService.rateCourse(mockCourse.getId(), mockStudent, 51);

        Mockito.verify(mockStudentCourseRepository,
                Mockito.times(1)).rateCourse(mockStudentCourse);
    }

    @Test
    void rateCourse_should_throw_whenAverageGradeIsBelow50() {
        User mockStudent = Helpers.createMockStudent();
        Course mockCourse = Helpers.createMockCourse();
        StudentCourse mockStudentCourse = new StudentCourse();

        Mockito.when(mockStudentCourseRepository.getStudentCourse(mockCourse.getId(), mockCourse.getId()))
                .thenReturn(mockStudentCourse);
        Mockito.when(mockStudentCourseRepository.getStudentCourseAverageGrade(mockCourse.getId(), mockStudent.getId()))
                .thenReturn(49d);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockStudentCourseService.rateCourse(mockCourse.getId(), mockStudent, 49));
    }

    @Test
    void isUserEnrolled_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();
        Course mockCourse = Helpers.createMockCourse();

        mockStudentCourseService.isUserEnrolled(mockCourse.getId(), mockStudent);

        Mockito.verify(mockStudentCourseRepository,
                Mockito.times(1)).isUserEnrolled(mockCourse.getId(), mockStudent);
    }

    @Test
    void isCourseRated_should_callRepository() {
        User mockStudent = Helpers.createMockStudent();
        Course mockCourse = Helpers.createMockCourse();

        mockStudentCourseService.isCourseRated(mockCourse.getId(), mockStudent);

        Mockito.verify(mockStudentCourseRepository,
                Mockito.times(1)).isCourseRated(mockCourse.getId(), mockStudent);
    }

}