package com.telerikacademy.web.chefmeup.services;

import com.telerikacademy.web.chefmeup.exeptions.DuplicateEntityException;
import com.telerikacademy.web.chefmeup.exeptions.NotAllowedOperationException;
import com.telerikacademy.web.chefmeup.exeptions.UnauthorizedOperationException;
import com.telerikacademy.web.chefmeup.exeptions.VisibilityNotAllowedOperationException;
import com.telerikacademy.web.chefmeup.models.Course;
import com.telerikacademy.web.chefmeup.models.Lecture;
import com.telerikacademy.web.chefmeup.models.StudentLecture;
import com.telerikacademy.web.chefmeup.models.User;
import com.telerikacademy.web.chefmeup.models.dtos.CourseSearchDto;
import com.telerikacademy.web.chefmeup.repositories.contracts.CourseRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.LectureRepository;
import com.telerikacademy.web.chefmeup.repositories.contracts.StudentLectureRepository;
import com.telerikacademy.web.chefmeup.services.contracts.StudentLectureService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.util.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;

import static com.telerikacademy.web.chefmeup.Helpers.*;


@ExtendWith(MockitoExtension.class)
public class CourseServiceImplTests {

    @Mock
    CourseRepository mockCourseRepository;
    @Mock
    LectureRepository mockLectureRepository;
    @Mock
    StudentLectureRepository mockStudentLectureRepository;

    @InjectMocks
    CourseServiceImpl mockCourseService;

    @Test
    public void getAll_should_callRepository() {
        Mockito.when(mockCourseRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockCourseService.getAll();

        Mockito.verify(mockCourseRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_should_callRepository() {
        Course mockCourse = createMockCourse();

        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        Course result = mockCourseService.getById(mockCourse.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCourse.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCourse.getTitle(), result.getTitle()),
                () -> Assertions.assertEquals(mockCourse.getTopic(), result.getTopic()),
                () -> Assertions.assertEquals(mockCourse.getDescription(), result.getDescription()),
                () -> Assertions.assertEquals(mockCourse.getCreator().getId(), result.getCreator().getId())
        );
    }

    @Test
    public void create_should_callRepository_when_userIsTeacher() {
        Course mockCourse = createMockCourse();
        User mockTeacher = createMockTeacher();

        mockCourseService.create(mockCourse, mockTeacher);

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).create(mockCourse);
    }

    @Test
    public void create_should_throw_when_courseTitleIsDuplicate() {
        Course mockCourse = createMockCourse();
        User mockTeacher = createMockTeacher();

        Mockito.when(mockCourseRepository.doesCourseTitleExist(mockCourse.getTitle(), 0))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockCourseService.create(mockCourse, mockTeacher));
    }

    @Test
    public void create_should_throw_when_startingDateIsAfterExpireDate() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();

        LocalDate startingDate = LocalDate.parse("2021-10-11");
        LocalDate expireDate = LocalDate.parse("2021-10-10");

        mockCourse.setStartingDate(startingDate);
        mockCourse.setExpireDate(expireDate);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockCourseService.create(mockCourse, mockTeacher));
    }

    @Test
    public void create_should_throw_when_startingDateIsExistsButExpireDoesnt() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();

        LocalDate startingDate = LocalDate.parse("2021-10-11");

        mockCourse.setStartingDate(startingDate);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockCourseService.create(mockCourse, mockTeacher));
    }

    @Test
    public void create_should_throw_when_expireDateIsExistsButStartingDoesnt() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();

        LocalDate expireDate = LocalDate.parse("2021-10-11");

        mockCourse.setExpireDate(expireDate);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockCourseService.create(mockCourse, mockTeacher));
    }

    @Test
    public void create_should_throw_when_courseDoesntHaveAtLeastTwoLectures() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();

        mockCourse.setVisible(true);

        Assertions.assertThrows(VisibilityNotAllowedOperationException.class,
                () -> mockCourseService.create(mockCourse, mockTeacher));
    }

    @Test
    public void update_should_callRepository_when_parametersAreCorrect() {
        Course mockCourse = createMockCourse();
        User mockTeacher = createMockTeacher();

        mockCourseService.update(mockCourse, mockTeacher);

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).update(mockCourse);
    }

    @Test
    public void update_should_throw_when_userIsNotCreatorOrAdmin() {
        Course mockCourse = createMockCourse();
        User mockTeacher = createMockTeacher();
        mockTeacher.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> mockCourseService.update(mockCourse, mockTeacher));
    }

    @Test
    public void update_should_throw_when_courseTitleIsDuplicate() {
        Course mockCourse = createMockCourse();
        User mockTeacher = createMockTeacher();

        Mockito.when(mockCourseRepository.doesCourseTitleExist(mockCourse.getTitle(), mockCourse.getId()))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockCourseService.update(mockCourse, mockTeacher));
    }

    @Test
    public void update_should_throw_when_startingDateIsAfterExpireDate() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();

        LocalDate startingDate = LocalDate.parse("2021-10-11");
        LocalDate expireDate = LocalDate.parse("2021-10-10");

        mockCourse.setStartingDate(startingDate);
        mockCourse.setExpireDate(expireDate);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockCourseService.update(mockCourse, mockTeacher));
    }

    @Test
    public void update_should_throw_when_courseIsSetOnlineButDoesntHaveStartingAndExpireDate() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();
        mockCourse.setVisible(true);

        Assertions.assertThrows(VisibilityNotAllowedOperationException.class,
                () -> mockCourseService.update(mockCourse, mockTeacher));
    }

    @Test
    public void update_should_throw_when_courseIsSetOnlineButDoesntHaveExpireDate() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();

        mockCourse.setVisible(true);
        LocalDate startingDate = LocalDate.parse("2021-10-11");
        mockCourse.setStartingDate(startingDate);

        Assertions.assertThrows(VisibilityNotAllowedOperationException.class,
                () -> mockCourseService.update(mockCourse, mockTeacher));
    }

    @Test
    public void update_should_throw_when_courseIsSetOnlineButDoesntHaveStartingDate() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();

        mockCourse.setVisible(true);
        LocalDate expireDate = LocalDate.parse("2021-10-11");
        mockCourse.setExpireDate(expireDate);

        Assertions.assertThrows(VisibilityNotAllowedOperationException.class,
                () -> mockCourseService.update(mockCourse, mockTeacher));
    }

    @Test
    public void update_should_throw_when_courseIsSetOnlineAndHasLessThatTwoLectures() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();
        mockCourse.setVisible(true);

        LocalDate startingDate = LocalDate.parse("2021-10-11");
        LocalDate expireDate = LocalDate.parse("2021-10-15");

        mockCourse.setStartingDate(startingDate);
        mockCourse.setExpireDate(expireDate);

        Mockito.when(mockLectureRepository.getAllCourseLectures(mockCourse.getId()))
                .thenReturn(new ArrayList<>());

        Assertions.assertThrows(VisibilityNotAllowedOperationException.class,
                () -> mockCourseService.update(mockCourse, mockTeacher));
    }

    @Test
    public void update_should_throw_when_courseIsAlreadyOnlineAndItsStarted() {
        User mockTeacher = createMockTeacher();
        Course mockCourse = createMockCourse();
        mockCourse.setVisible(true);
        List<Lecture> lectures = new ArrayList<>();
        Lecture lectureOne = createMockLecture();
        Lecture lectureTwo = createMockLecture();
        lectureOne.setId(2);

        lectures.add(lectureOne);
        lectures.add(lectureTwo);

        LocalDate startingDate = LocalDate.parse("2020-10-01");
        LocalDate expireDate = LocalDate.parse("2023-10-15");

        mockCourse.setStartingDate(startingDate);
        mockCourse.setExpireDate(expireDate);

        Mockito.when(mockLectureRepository.getAllCourseLectures(mockCourse.getId()))
                .thenReturn(lectures);

        Assertions.assertThrows(VisibilityNotAllowedOperationException.class,
                () -> mockCourseService.update(mockCourse, mockTeacher));
    }


    @Test
    public void delete_should_callRepository_whenInformationIsValid() {
        Course mockCourse = createMockCourse();

        Mockito.when(mockCourseService.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        mockCourseService.delete(mockCourse.getId());

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).delete(mockCourse.getId());
    }

    @Test
    public void delete_should_throw_whenThereAreLecturesInTheCourse() {
        Course mockCourse = createMockCourse();
        List<Lecture> lectures = new ArrayList<>();
        Lecture lecture = createMockLecture();
        lectures.add(lecture);

        Mockito.when(mockLectureRepository.getAllCourseLectures(mockCourse.getId()))
                .thenReturn(lectures);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockCourseService.delete(mockCourse.getId()));
    }

    @Test
    public void delete_should_throw_whenCourseIsOnlineAndItsStarted() {
        Course mockCourse = createMockCourse();
        List<Lecture> lectures = new ArrayList<>();
        Lecture lecture = createMockLecture();
        lectures.add(lecture);

        Mockito.when(mockLectureRepository.getAllCourseLectures(mockCourse.getId()))
                .thenReturn(lectures);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockCourseService.delete(mockCourse.getId()));
    }

    @Test
    public void delete_should_throw_whenCourseIsStartedAlready() {
        Course mockCourse = createMockCourse();
        mockCourse.setVisible(true);

        LocalDate startingDate = LocalDate.parse("2020-10-01");
        LocalDate expireDate = LocalDate.parse("2023-10-15");

        mockCourse.setStartingDate(startingDate);
        mockCourse.setExpireDate(expireDate);

        Mockito.when(mockLectureRepository.getAllCourseLectures(mockCourse.getId()))
                .thenReturn(new ArrayList<>());
        Mockito.when(mockCourseService.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> mockCourseService.delete(mockCourse.getId()));
    }

    @Test
    public void getAllTeacherDraftCourses_should_callRepository() {
        User mockTeacher = createMockTeacher();

        mockCourseService.getAllTeacherDraftCourses(mockTeacher);

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).getAllTeacherDraftCourses(mockTeacher);
    }

    @Test
    public void getAllTeacherCourse_should_callRepositoryWhenUserIsTeacher() {
        User mockTeacher = createMockTeacher();

        mockCourseService.getAllTeacherCourses(mockTeacher);

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).getAllTeacherCourses(mockTeacher);
    }

    @Test
    public void getAllTeacherCourse_should_callRepositoryWhenUserIsAdmin() {
        User mockAdmin = createMockAdmin();

        mockCourseService.getAllTeacherCourses(mockAdmin);

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).getAll();
    }

    @Test
    public void getAllUsersEntolledCourses_should_callRepository() {
        User mockStudent = createMockStudent();

        mockCourseService.getAllUsersEnrolledCourses(mockStudent);

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).getAllUsersEnrolledCourses(mockStudent);
    }

    @Test
    public void getAllCourseRating_should_returnMap() {
        Map<Integer, String> map = new HashMap<>();

        Mockito.when(mockCourseRepository.getAll())
                .thenReturn(new ArrayList<>());

        Assertions.assertEquals(mockCourseService.getAllCourseRatings(), map);
    }

    @Test
    public void getAllCourseRating_should_returnMapWithEqualValues() {
        Course mockCourse = createMockCourse();
        List<Course> courses = new ArrayList<>();
        courses.add(mockCourse);

        Mockito.when(mockCourseRepository.getAll())
                .thenReturn(courses);
        Mockito.when(mockCourseRepository.getCourseAverageRating(mockCourse.getId()))
                .thenReturn(0.0);

        Assertions.assertEquals(mockCourseService.getAllCourseRatings().get(mockCourse.getId()), "No rating yet.");
    }

    @Test
    public void getAllCourseRating_should_returnMapWithCorrectValue() {
        Course mockCourse = createMockCourse();
        List<Course> courses = new ArrayList<>();
        courses.add(mockCourse);

        Mockito.when(mockCourseRepository.getAll())
                .thenReturn(courses);
        Mockito.when(mockCourseRepository.getCourseAverageRating(mockCourse.getId()))
                .thenReturn(4.4);

        Assertions.assertEquals(mockCourseService.getAllCourseRatings().get(mockCourse.getId()), "Rating: 4.4/5");
    }

    @Test
    public void getUsersCourseGrade_should_returnProperGrades() {
        User mockStudent = createMockStudent();
        Course mockCourse = createMockCourse();
        List<Course> courses = new ArrayList<>();
        courses.add(mockCourse);

        Lecture lecture = createMockLecture();
        List<Lecture> lectures = new ArrayList<>();
        lectures.add(lecture);

        StudentLecture studentLecture = new StudentLecture();
        studentLecture.setGrade(80);

        Mockito.when(mockCourseRepository.getAllUsersEnrolledCourses(mockStudent))
                .thenReturn(courses);
        Mockito.when(mockLectureRepository.getAllCourseLectures(mockCourse.getId()))
                .thenReturn(lectures);
        Mockito.when(mockStudentLectureRepository.getByStudentAndLecture(mockStudent.getId(), lecture.getId()))
                .thenReturn(studentLecture);

        Assertions.assertEquals(mockCourseService.getUsersCourseGrade(mockStudent).get(mockCourse.getId()), "80,0");
    }

    @Test
    public void uploadPicture_should_callRepository() {
        Course mockCourse = createMockCourse();
        User mockTeacher = createMockTeacher();
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String saveAsName = mockCourse.getTitle() + "-" +
                "BACKGROUND" + "." +
                fileName.substring(fileName.lastIndexOf(".") + 1);
        String name = saveAsName.replaceAll(" ", "");
        Path resourceDirectory = Paths.get("media", "coursebackgrounds", name);
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        Path assignments = Paths.get(absolutePath);

        mockCourseService.uploadCoursePicture(file, mockCourse, mockTeacher);

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).uploadCoursePicture(file, mockCourse, name, assignments);

    }

    @Test
    public void getCourseAverageRating_should_callRepository() {
        Course mockCourse = createMockCourse();

        mockCourseService.getCourseAverageRating(mockCourse.getId());

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).getCourseAverageRating(mockCourse.getId());
    }

    @Test
    public void search_should_callRepository() {
        CourseSearchDto courseSearchDto = new CourseSearchDto();

        mockCourseService.search(courseSearchDto);

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).search(courseSearchDto);
    }

    @Test
    public void isCourseStarted_should_returnTrueIfCourseIsStarted() {
        Course mockCourse = createMockCourse();

        Mockito.when(mockCourseRepository.getById(mockCourse.getId()))
                .thenReturn(mockCourse);

        LocalDate startingDate = LocalDate.parse("2020-10-01");
        LocalDate expireDate = LocalDate.parse("2023-10-15");

        mockCourse.setStartingDate(startingDate);
        mockCourse.setExpireDate(expireDate);

        Assertions.assertTrue(mockCourseService.isCourseStarted(mockCourse.getId()));
    }

    @Test
    public void getAllOnlineCourses_should_callRepository() {
        mockCourseService.getAllOnlineCourses();

        Mockito.verify(mockCourseRepository,
                Mockito.times(1)).getAllOnlineCourses();
    }

}
