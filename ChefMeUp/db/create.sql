-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.3-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for chef_me_up
CREATE DATABASE IF NOT EXISTS `chef_me_up` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `chef_me_up`;

-- Dumping structure for table chef_me_up.countries
CREATE TABLE IF NOT EXISTS `countries` (
                                           `sortname` varchar(45) NOT NULL,
                                           `country_id` int(11) NOT NULL AUTO_INCREMENT,
                                           `name` varchar(45) NOT NULL,
                                           `phonecode` int(11) NOT NULL,
                                           PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table chef_me_up.courses
CREATE TABLE IF NOT EXISTS `courses` (
                                         `course_id` int(11) NOT NULL AUTO_INCREMENT,
                                         `creator_id` int(11) NOT NULL,
                                         `title` varchar(50) NOT NULL,
                                         `description` text DEFAULT NULL,
                                         `starting_date` date DEFAULT NULL,
                                         `expire_date` date DEFAULT NULL,
                                         `topic_id` int(11) NOT NULL,
                                         `visible` tinyint(1) NOT NULL DEFAULT 0,
                                         `picture` text DEFAULT NULL,
                                         `video` text DEFAULT NULL,
                                         PRIMARY KEY (`course_id`),
                                         UNIQUE KEY `courses_title_uindex` (`title`),
                                         KEY `courses_users_user_id_fk` (`creator_id`),
                                         KEY `courses_topics_topic_id_fk` (`topic_id`),
                                         KEY `courses_course_statuses_course_status_id_fk` (`visible`),
                                         CONSTRAINT `courses_topics_topic_id_fk` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`topic_id`),
                                         CONSTRAINT `courses_users_user_id_fk` FOREIGN KEY (`creator_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table chef_me_up.lectures
CREATE TABLE IF NOT EXISTS `lectures` (
                                          `description` text DEFAULT NULL,
                                          `lecture_id` int(11) NOT NULL AUTO_INCREMENT,
                                          `title` varchar(50) NOT NULL,
                                          `video` varchar(45) NOT NULL,
                                          `assigment` varchar(100) DEFAULT NULL,
                                          `course_id` int(11) NOT NULL,
                                          `picture` text DEFAULT NULL,
                                          PRIMARY KEY (`lecture_id`),
                                          KEY `lectures_courses_course_id_fk` (`course_id`),
                                          CONSTRAINT `lectures_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table chef_me_up.roles
CREATE TABLE IF NOT EXISTS `roles` (
                                       `role_id` int(11) NOT NULL AUTO_INCREMENT,
                                       `name` varchar(45) NOT NULL,
                                       PRIMARY KEY (`role_id`),
                                       UNIQUE KEY `roles_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table chef_me_up.students_courses
CREATE TABLE IF NOT EXISTS `students_courses` (
                                                  `id` int(11) NOT NULL AUTO_INCREMENT,
                                                  `student_id` int(11) NOT NULL,
                                                  `course_id` int(11) NOT NULL,
                                                  `course_rating` int(11) DEFAULT NULL,
                                                  PRIMARY KEY (`id`),
                                                  KEY `students_courses_courses_course_id_fk` (`course_id`),
                                                  KEY `students_courses_users_user_id_fk` (`student_id`),
                                                  CONSTRAINT `students_courses_courses_course_id_fk` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`),
                                                  CONSTRAINT `students_courses_users_user_id_fk` FOREIGN KEY (`student_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table chef_me_up.students_lectures
CREATE TABLE IF NOT EXISTS `students_lectures` (
                                                   `completed` tinyint(1) NOT NULL DEFAULT 0,
                                                   `id` int(11) NOT NULL AUTO_INCREMENT,
                                                   `student_id` int(11) NOT NULL,
                                                   `assignment` text DEFAULT NULL,
                                                   `lecture_id` int(11) DEFAULT NULL,
                                                   `grade` int(11) DEFAULT 0,
                                                   PRIMARY KEY (`id`),
                                                   KEY `students_lectures_lectures_lecture_id_fk` (`lecture_id`),
                                                   KEY `students_lectures_users_user_id_fk` (`student_id`),
                                                   CONSTRAINT `students_lectures_lectures_lecture_id_fk` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`),
                                                   CONSTRAINT `students_lectures_users_user_id_fk` FOREIGN KEY (`student_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table chef_me_up.topics
CREATE TABLE IF NOT EXISTS `topics` (
                                        `topic_id` int(11) NOT NULL AUTO_INCREMENT,
                                        `name` varchar(45) NOT NULL,
                                        PRIMARY KEY (`topic_id`),
                                        UNIQUE KEY `topics_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table chef_me_up.users
CREATE TABLE IF NOT EXISTS `users` (
                                       `user_id` int(11) NOT NULL AUTO_INCREMENT,
                                       `e_mail` varchar(45) NOT NULL,
                                       `first_name` varchar(45) NOT NULL,
                                       `last_name` varchar(45) NOT NULL,
                                       `phone` varchar(15) NOT NULL,
                                       `country_id` int(11) NOT NULL,
                                       `picture` varchar(45) NOT NULL,
                                       `teacher_request` tinyint(1) NOT NULL DEFAULT 0,
                                       `password` varchar(45) NOT NULL,
                                       PRIMARY KEY (`user_id`),
                                       UNIQUE KEY `users_e_mail_uindex` (`e_mail`),
                                       UNIQUE KEY `users_phone_uindex` (`phone`),
                                       KEY `users_countries_country_id_fk` (`country_id`),
                                       CONSTRAINT `users_countries_country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `countries` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table chef_me_up.users_roles
CREATE TABLE IF NOT EXISTS `users_roles` (
                                             `user_id` int(11) NOT NULL,
                                             `role_id` int(11) NOT NULL,
                                             KEY `users_roles_roles_role_id_fk` (`role_id`),
                                             KEY `users_roles_users_user_id_fk` (`user_id`),
                                             CONSTRAINT `users_roles_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
                                             CONSTRAINT `users_roles_users_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
