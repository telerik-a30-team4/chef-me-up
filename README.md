<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg)" alt="logo" width="300px" style="margin-top: 20px;"/>

# Chef Me Up
Telerik Alpha 30 - final project.
### By Dimitar Bonchev and Dimo Sotirov

![picture](images/chefmeup-logo.png)

## Project Description
This is a web application for online cooking classes.<br/>
It provides functionality based on the role of the user that is using it.
ChefMeUp has four types of users - anonymous, students, teachers and admins.<br/>

### Public Part - Anonymous users

The public part is visible without authentication.<br/>
Anonymous users are able to register as students and the ability to request becoming a teacher.<br/>
The anonymous users can to browse the catalog of available courses and filter it by course name, course topic and teacher,
as well as sorting the catalog by name.<br/>
They are not able to enroll for a course.<br/>

### Private part - Students

It is accessible only if the student is authenticated.<br/>
They have private section (profile page).<br/>
They have the following functionality in the application:<br/>
• view and edit their profile<br/>
• change their password<br/>
• enroll for courses<br/>
• see their enrolled and completed courses<br/>

### Private part - Teachers

Teachers have access to all the functionality that students do.<br/>
They can be approved only by an administrator.<br/>
They have the following functionality in the application:<br/>
• creating courses, adding video lectures and assignments to them<br/>
• evaluate students assignments and give a grade <br/>
• browse list of students and teachers<br/>

Courses can be either draft or published. Draft courses are visible to the teachers, but
not to the students. Once the teacher has prepared the course, they are able to
publish it and it becomes available to all students for enrollment.<br/>

### Private part - Admins

Administrators have access to all the functionality that teachers do.<br/>
They cannot be registered though the ordinary registration process.<br/>
They can modify courses, lectures, user profiles and approve teachers.<br/>

### REST API
To provide other developers with our service, we have developed a REST API.<br/>
[Link to the Swagger documentation](http://localhost:8080/swagger-ui.html) <br/>

### GitLab
[Link to our project](https://gitlab.com/telerik-a30-team4/chef-me-up) <br/>

### Database relations
![picture](images/chefmeup-database-relations.png) 